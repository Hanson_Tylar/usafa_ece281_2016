# Lab #3 - Elevator Controller Variations

## By C3C Tylar Hanson

## Table of Contents
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Preliminary Design](#preliminary-design)
3. [Well-formatted code](#well-formatted-code)
4. [Debugging](#debugging)
5. [Testing methodology or results](#testing-methodology-or-results)
6. [Observations and Conclusions](#observations-and-conclusions)
7. [Documentation](#documentation)
 
### Objectives or Purpose 
I will test and debug the elevator controller built in computer exercise 3 by synthesizing and configuring an FPGA with my state machine. I will then practice implementing state machines in VHDL by modifying my controller code to implement different variants of elevator controllers. 

### Preliminary design
Prelab approach. Start early and follow outlined steps. By outlining what I believe to be happening in the top schematic I have a better approach to this lab. The switches control what floor the elevator goes to, the LEDs flash indicating which way the elevator moves, and the Nexys2_sseg and nibble_to_sseg pieces turn on the seven segment display in accordance with the floor number given. 

![](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/bc00448e120ce3e2c0bf24b16284fed5b21403c4/Lab3_Hanson/Prelab_schematic.jpg?token=95ecfa4c49e3763ca34df95d6ee2426e877d53f7)

### Well-formatted code
Below is the top shell code
  --------------------------------------------------------------------
	-- Name: Tylar Hanson
	-- Date: 28 MAR 2016
	-- Course: ECE 281
	-- File: Nexys2_top_shell.vhd
	-- HW: Lab #3 - Elevator Controller
	--
	-- Purp: Design an elevator controller with several variations 
	--
	-- Doc: None.
	--
	-- Academic Integrity Statement: I certify that, while others may have 
	-- assisted me in brain storming, debugging and validating this program, 
	-- the program itself is my own work. I understand that submitting code 
	-- which is the work of other individuals is a violation of the honor   
	-- code.  I also understand that if I knowingly give my original work to 
	-- another individual is also a violation of the honor code. 
	------------------------------------------------------------------------- 
	library IEEE;
	use IEEE.STD_LOGIC_1164.ALL;
	
	-- Uncomment the following library declaration if using
	-- arithmetic functions with Signed or Unsigned values
	--use IEEE.NUMERIC_STD.ALL;
	
	-- Uncomment the following library declaration if instantiating
	-- any Xilinx primitives in this code.
	--library UNISIM;
	--use UNISIM.VComponents.all;
	
	entity Nexys2_top_shell is
	    Port ( 	clk_50m : in STD_LOGIC;
					btn : in  STD_LOGIC_VECTOR (3 DOWNTO 0);
					switch : in STD_LOGIC_VECTOR (7 DOWNTO 0);
					SSEG_AN : out STD_LOGIC_VECTOR (3 DOWNTO 0);
					SSEG : out STD_LOGIC_VECTOR (7 DOWNTO 0);
					LED : out STD_LOGIC_VECTOR (7 DOWNTO 0));
	end Nexys2_top_shell;
	
	architecture Behavioral of Nexys2_top_shell is
	
	---------------------------------------------------------------------------------------
	--This component converts a nibble to a value that can be viewed on a 7-segment display
	--Similar in function to a 7448 BCD to 7-seg decoder
	--Inputs: 4-bit vector called "nibble"
	--Outputs: 8-bit vector "sseg" used for driving a single 7-segment display
	---------------------------------------------------------------------------------------
		COMPONENT nibble_to_sseg
		PORT(
			nibble : IN std_logic_vector(3 downto 0);          
			sseg : OUT std_logic_vector(7 downto 0)
			);
		END COMPONENT;
	
	---------------------------------------------------------------------------------------------
	--This component manages the logic for displaying values on the NEXYS 2 7-segment displays
	--Inputs: system clock, synchronous reset, 4 8-bit vectors from 4 instances of nibble_to_sseg
	--Outputs: 7-segment display select signal (4-bit) called "sel", 
	--         8-bit signal called "sseg" containing 7-segment data routed off-chip
	---------------------------------------------------------------------------------------------
		COMPONENT nexys2_sseg
		GENERIC ( CLOCK_IN_HZ : integer );
		PORT(
			clk : IN std_logic;
			reset : IN std_logic;
			sseg0 : IN std_logic_vector(7 downto 0);
			sseg1 : IN std_logic_vector(7 downto 0);
			sseg2 : IN std_logic_vector(7 downto 0);
			sseg3 : IN std_logic_vector(7 downto 0);          
			sel : OUT std_logic_vector(3 downto 0);
			sseg : OUT std_logic_vector(7 downto 0)
			);
		END COMPONENT;
	
	-------------------------------------------------------------------------------------
	--This component divides the system clock into a bunch of slower clock speeds
	--Input: system clock 
	--Output: 27-bit clockbus. Reference module for the relative clock speeds of each bit
	--			 assuming system clock is 50MHz
	-------------------------------------------------------------------------------------
		COMPONENT Clock_Divider
		PORT(
			clk : IN std_logic;          
			clockbus : OUT std_logic_vector(26 downto 0)
			);
		END COMPONENT;
	
	-------------------------------------------------------------------------------------
	--Below are declarations for signals that wire-up this top-level module.
	-------------------------------------------------------------------------------------
	
	signal nibble0, nibble1, nibble2, nibble3 : std_logic_vector(3 downto 0);
	signal sseg0_sig, sseg1_sig, sseg2_sig, sseg3_sig : std_logic_vector(7 downto 0);
	signal ClockBus_sig : STD_LOGIC_VECTOR (26 downto 0);
	
	
	--------------------------------------------------------------------------------------
	--Insert your design's component declaration below	
	--------------------------------------------------------------------------------------
	
	--COMPONENT MooreElevatorController_Shell
	--	PORT (
	--		clk : in  STD_LOGIC;
	--		reset : in  STD_LOGIC;
	--		destination : in  STD_LOGIC_VECTOR (2 downto 0);
	--		floor : out  STD_LOGIC_VECTOR (3 downto 0);
	--		floor_light : out STD_LOGIC_VECTOR (7 downto 0));
	--END COMPONENT;
	
	--COMPONENT MealyElevatorController_Shell
	--	PORT(
	--		clk : IN std_logic;
	--		reset : IN std_logic;
	--		stop : IN std_logic;
	--		up_down : IN std_logic;          
	--		floor : OUT std_logic_vector(3 downto 0);
	--		nextfloor : OUT std_logic_vector(3 downto 0)
	--		);
	--END COMPONENT;
	
	COMPONENT Moore_Prime_Floors
		PORT(
			clk : IN std_logic;
			reset : IN std_logic;
			stop : IN std_logic;
			up_down : IN std_logic;          
			floor : OUT std_logic_vector(3 downto 0);
			carry : OUT std_logic_vector(3 downto 0)
			);
	END COMPONENT;
		
	--------------------------------------------------------------------------------------
	--Insert any required signal declarations below
	--------------------------------------------------------------------------------------
	
	begin
	
	----------------------------
	--code below tests the LEDs:
	----------------------------
	
	LED <= CLOCKBUS_SIG(26 DOWNTO 19);
	
	--------------------------------------------------------------------------------------------	
	--This code instantiates the Clock Divider. Reference the Clock Divider Module for more info
	--------------------------------------------------------------------------------------------
		Clock_Divider_Label: Clock_Divider PORT MAP(
			clk => clk_50m,
			clockbus => ClockBus_sig
		);
	
	--------------------------------------------------------------------------------------	
	--Code below drives the function of the 7-segment displays. 
	--Function: To display a value on 7-segment display #0, set the signal "nibble0" to 
	--				the value you wish to display
	--				To display a value on 7-segment display #1, set the signal "nibble1" to 
	--				the value you wish to display...and so on
	--Note: You must set each "nibble" signal to a value. 
	--		  Example: if you are not using 7-seg display #3 set nibble3 to "0000"
	--------------------------------------------------------------------------------------
	
	--nibble0 <= 
	--nibble1 <= 
	--nibble2 <= 
	--nibble3 <= 
	
	--This code converts a nibble to a value that can be displayed on 7-segment display #0
		sseg0: nibble_to_sseg PORT MAP(
			nibble => nibble0,
			sseg => sseg0_sig
		);
	
	--This code converts a nibble to a value that can be displayed on 7-segment display #1
		sseg1: nibble_to_sseg PORT MAP(
			nibble => nibble1,
			sseg => sseg1_sig
		);
	
	--This code converts a nibble to a value that can be displayed on 7-segment display #2
		sseg2: nibble_to_sseg PORT MAP(
			nibble => nibble2,
			sseg => sseg2_sig
		);
	
	--This code converts a nibble to a value that can be displayed on 7-segment display #3
		sseg3: nibble_to_sseg PORT MAP(
			nibble => nibble3,
			sseg => sseg3_sig
		);
		
	--This module is responsible for managing the 7-segment displays, you don't need to do anything here
		nexys2_sseg_label: nexys2_sseg 
		generic map ( CLOCK_IN_HZ => 50E6 )
		PORT MAP(
			clk => clk_50m,
			reset => '0',
			sseg0 => sseg0_sig,
			sseg1 => sseg1_sig,
			sseg2 => sseg2_sig,
			sseg3 => sseg3_sig,
			sel => SSEG_AN,
			sseg => SSEG
		);
		
	-----------------------------------------------------------------------------
	--Instantiate the design you with to implement below and start wiring it up!:
	-----------------------------------------------------------------------------
	
	--	MooreElevatorController_Shell_1: MooreElevatorController_Shell PORT MAP(
	--		clk => ClockBus_sig(25),
	--		reset => btn(3),
	--		destination => switch(2 downto 0),
	--		floor => nibble0,
	--		floor_light => LED
	--	);
		
	--	MooreElevatorController_Shell_2: MooreElevatorController_Shell PORT MAP(
	--		clk => ClockBus_sig(25),
	--		reset => btn(3),
	--		destination => switch(5 downto 3),
	--		floor => nibble2
	--	);
	
	Moore_Prime_Floors_1: Moore_Prime_Floors PORT MAP(
			clk => ClockBus_sig(25),
			reset => btn(3),
			stop => btn(2),
			up_down => btn(1),
			floor => nibble0,
			carry => nibble1
		);
		
	end Behavioral;  

This is the Moore Elevator Controller Code

	  --------------------------------------------------------------------
		-- Name: Tylar Hanson
		-- Date: 28 MAR 2016
		-- Course: ECE 281
		-- File: MooreElevatorController_shell.vhd
		-- HW: Lab #3 - Elevator Controller
		--
		-- Purp: Design an elevator controller with several variations 
		--
		-- Doc: None.
		--
		-- Academic Integrity Statement: I certify that, while others may have 
		-- assisted me in brain storming, debugging and validating this program, 
		-- the program itself is my own work. I understand that submitting code 
		-- which is the work of other individuals is a violation of the honor   
		-- code.  I also understand that if I knowingly give my original work to 
		-- another individual is also a violation of the honor code. 
		------------------------------------------------------------------------- 
	library IEEE;
	use IEEE.STD_LOGIC_1164.ALL;
	
	-- Uncomment the following library declaration if using
	-- arithmetic functions with Signed or Unsigned values
	--use IEEE.NUMERIC_STD.ALL;
	
	-- Uncomment the following library declaration if instantiating
	-- any Xilinx primitives in this code.
	--library UNISIM;
	--use UNISIM.VComponents.all;
	
	entity MooreElevatorController_Shell is
	    Port ( clk : in  STD_LOGIC;
	           reset : in  STD_LOGIC;
	           destination : in  STD_LOGIC_VECTOR (2 downto 0);
				  floor_light : out STD_LOGIC_VECTOR (7 downto 0);
	           floor : out  STD_LOGIC_VECTOR (3 downto 0));
	end MooreElevatorController_Shell;
	
	architecture Behavioral of MooreElevatorController_Shell is
	
	--Below you create a new variable type! You also define what values that 
	--variable type can take on. Now you can assign a signal as 
	--"floor_state_type" the same way you'd assign a signal as std_logic 
	type floor_state_type is (floor0, floor1, floor2, floor3, floor4, floor5, floor6, floor7);
	
	--Here you create a variable "floor_state" that can take on the values
	--defined above. Neat-o!
	signal floor_state : floor_state_type;
	
	begin
	---------------------------------------------
	--Below you will code your next-state process
	---------------------------------------------
	
	--This line will set up a process that is sensitive to the clock
	floor_state_machine: process(clk)
	begin
		if rising_edge(clk) then
			if reset='1' then
				floor_state <= floor1;
			--now we will code our next-state logic
				else
				case floor_state is
				
					when floor0 =>
						if (destination > "000") then 
							floor_state <= floor1;
						else
							floor_state <= floor0;
						end if;
						
					when floor1 => 
						if (destination > "001") then 
							floor_state <= floor2;
						elsif (destination < "001") then 
							floor_state <= floor0;
						else
							floor_state <= floor1;
						end if;
						
					when floor2 => 
						if (destination > "010") then 
							floor_state <= floor3;
						elsif (destination < "010") then 
							floor_state <= floor1;
						else
							floor_state <= floor2;
						end if;
					
					when floor3 =>
						if (destination > "011") then 
							floor_state <= floor4;
						elsif (destination < "011") then 
							floor_state <= floor2;
						else
							floor_state <= floor3;
						end if;
						
					when floor4 =>
						if (destination > "100") then 
							floor_state <= floor5;
						elsif (destination < "100") then 
							floor_state <= floor3;
						else
							floor_state <= floor4;
						end if;
						
					when floor5 =>
						if (destination > "101") then 
							floor_state <= floor6;
						elsif (destination < "101") then 
							floor_state <= floor4;
						else
							floor_state <= floor5;
						end if;
						
					when floor6 =>
						if (destination > "110") then 
							floor_state <= floor7;
						elsif (destination < "110") then 
							floor_state <= floor5;
						else
							floor_state <= floor6;
						end if;
						
					when floor7 =>
						if (destination >= "111") then 
							floor_state <= floor7;
						else 
							floor_state <= floor6;
						end if;
					
					--This line accounts for phantom states
					when others =>
						floor_state <= floor1;
				end case;
			end if;
		end if;
	end process;
	
	-- Here you define your output logic. Finish the statements below
	floor <= "0000" when (floor_state = floor0) else
				"0001" when (floor_state = floor1) else
				"0010" when (floor_state = floor2) else
				"0011" when (floor_state = floor3) else
				"0100" when (floor_state = floor4) else
				"0101" when (floor_state = floor5) else
				"0110" when (floor_state = floor6) else
				"0111" when (floor_state = floor7) else
				"0001";
	
	floor_light <= "00000001" when (floor_state=floor0) else
						"00000010" when (floor_state=floor1) else
						"00000100" when (floor_state=floor2) else
						"00001000" when (floor_state=floor3) else
						"00010000" when (floor_state=floor4) else
						"00100000" when (floor_state=floor5) else
						"01000000" when (floor_state=floor6) else
						"10000000" when (floor_state=floor7) else 
						"00000000";
	end Behavioral;  

The next file is the Mealy Elevator controller.

	 --------------------------------------------------------------------
		-- Name: Tylar Hanson
		-- Date: 28 MAR 2016
		-- Course: ECE 281
		-- File: MealyElevatorController_Shell.vhd
		-- HW: Lab #3 - Elevator Controller Variations
		--
		-- Purp: Design an elevator controller with several variations 
		--
		-- Doc: None
		--
		-- Academic Integrity Statement: I certify that, while others may have 
		-- assisted me in brain storming, debugging and validating this program, 
		-- the program itself is my own work. I understand that submitting code 
		-- which is the work of other individuals is a violation of the honor   
		-- code.  I also understand that if I knowingly give my original work to 
		-- another individual is also a violation of the honor code. 
		-------------------------------------------------------------------------
	library IEEE;
	use IEEE.STD_LOGIC_1164.ALL;
	
	-- Uncomment the following library declaration if using
	-- arithmetic functions with Signed or Unsigned values
	--use IEEE.NUMERIC_STD.ALL;
	
	-- Uncomment the following library declaration if instantiating
	-- any Xilinx primitives in this code.
	--library UNISIM;
	--use UNISIM.VComponents.all;
	
	entity MealyElevatorController_Shell is
	    Port ( clk : in  STD_LOGIC;
	           reset : in  STD_LOGIC;
	           stop : in  STD_LOGIC;
	           up_down : in  STD_LOGIC;
	           floor : out  STD_LOGIC_VECTOR (3 downto 0);
				  nextfloor : out std_logic_vector (3 downto 0));
	end MealyElevatorController_Shell;
	
	architecture Behavioral of MealyElevatorController_Shell is
	
	type floor_state_type is (floor1, floor2, floor3, floor4);
	
	signal floor_state : floor_state_type;
	
	begin
	
	---------------------------------------------------------
	--Code your Mealy machine next-state process below
	--Question: Will it be different from your Moore Machine?
	---------------------------------------------------------
	floor_state_machine: process(clk)
	begin
		if rising_edge(clk) then
			--reset is active high and will return the elevator to floor1
			--Question: is reset synchronous or asynchronous?
			if reset='1' then
				floor_state <= floor1;
			--now we will code our next-state logic
			else
				case floor_state is
					--when our current state is floor1
					when floor1 =>
						--if up_down is set to "go up" and stop is set to 
						--"don't stop" which floor do we want to go to?
						if (up_down='1' and stop='0') then 
							--floor2 right?? This makes sense!
							floor_state <= floor2;
						--otherwise we're going to stay at floor1
						else
							floor_state <= floor1;
						end if;
					--when our current state is floor2
					when floor2 => 
						--if up_down is set to "go up" and stop is set to
						--"don't stop" which floor do we want to go to?
						if (up_down='1' and stop='0') then 
							floor_state <= floor3;
						--if up_down is set to "go down" and stop is set to 
						--"don't stop" which floor do we want to go to?
						elsif (up_down='0' and stop='0') then 
							floor_state <= floor1;
						--otherwise we're going to stay at floor2
						else
							floor_state <= floor2;
						end if;
					when floor3 =>
						if (up_down='1' and stop='0') then 
							floor_state <= floor4;
						elsif (up_down='0' and stop='0') then 
							floor_state <= floor2;
						else
							floor_state <= floor3;
						end if;
					when floor4 =>
						if (up_down='0' and stop='0') then 
							floor_state <= floor3;
						else 
							floor_state <= floor4;
						end if;
					
					--This line accounts for phantom states
					when others =>
						floor_state <= floor1;
				end case;
			end if;
		end if;
	end process;
	
	-----------------------------------------------------------
	--Code your Ouput Logic for your Mealy machine below
	--Remember, now you have 2 outputs (floor and nextfloor)
	-----------------------------------------------------------
	floor <= "0001" when (floor_state = floor1) else
				"0010" when (floor_state = floor2) else
				"0011" when (floor_state = floor3) else
				"0100" when (floor_state = floor4) else
				"0001";
				
	nextfloor <= "0001" when ((reset = '1') or (floor_state = floor1 and stop = '1') or (floor_state = floor1 and up_down = '0' and stop = '0') or (floor_state = floor2 and up_down = '0' and stop = '0')) else
					 "0010" when ((floor_state = floor2 and stop = '1') or (floor_state = floor1 and up_down = '1' and stop = '0') or (floor_state = floor3 and up_down = '0' and stop = '0')) else
					 "0011" when ((floor_state = floor3 and stop = '1') or (floor_state = floor2 and up_down = '1' and stop = '0') or (floor_state = floor4 and up_down = '0' and stop = '0')) else
					 "0100" when ((floor_state = floor4 and stop = '1') or (floor_state = floor3 and up_down = '1' and stop = '0') or (floor_state = floor4 and up_down = '1' and stop = '0')) else
					 "0001";
	
	end Behavioral;   

This code is the code for the prime floors functionality.

	  	--------------------------------------------------------------------
		-- Name: Tylar Hanson
		-- Date: 4 APR 2016
		-- Course: ECE 281
		-- File: Moore_Prime_Floors.vhd
		-- HW: Lab #3 - Elevator Controller Variations
		--
		-- Purp: Design an elevator controller that will traverse eight
		-- 		prime numbered floors.
		--
		-- Doc: None.
		--
		-- Academic Integrity Statement: I certify that, while others may have 
		-- assisted me in brain storming, debugging and validating this program, 
		-- the program itself is my own work. I understand that submitting code 
		-- which is the work of other individuals is a violation of the honor   
		-- code.  I also understand that if I knowingly give my original work to 
		-- another individual is also a violation of the honor code. 
		------------------------------------------------------------------------- 
	library IEEE;
	use IEEE.STD_LOGIC_1164.ALL;
	
	-- Uncomment the following library declaration if using
	-- arithmetic functions with Signed or Unsigned values
	--use IEEE.NUMERIC_STD.ALL;
	
	-- Uncomment the following library declaration if instantiating
	-- any Xilinx primitives in this code.
	--library UNISIM;
	--use UNISIM.VComponents.all;
	
	entity Moore_Prime_Floors is
	    Port ( clk : in  STD_LOGIC;
	           reset : in  STD_LOGIC;
	           stop : in  STD_LOGIC;
	           up_down : in  STD_LOGIC;
	           floor : out  STD_LOGIC_VECTOR (3 downto 0);
				  carry : out  STD_LOGIC_VECTOR (3 downto 0));
	end Moore_Prime_Floors;
	
	architecture Behavioral of Moore_Prime_Floors is
	
	--Below you create a new variable type! You also define what values that 
	--variable type can take on. Now you can assign a signal as 
	--"floor_state_type" the same way you'd assign a signal as std_logic 
	type floor_state_type is (floor1, floor2, floor3, floor4, floor5, floor6, floor7, floor8);
	
	--Here you create a variable "floor_state" that can take on the values
	--defined above. Neat-o!
	signal floor_state : floor_state_type;
	
	begin
	---------------------------------------------
	--Below you will code your next-state process
	---------------------------------------------
	
	--This line will set up a process that is sensitive to the clock
	floor_state_machine: process(clk)
	begin
		--clk'event and clk='1' is VHDL-speak for a rising edge
		-- if clk'event and clk='1' then
		if rising_edge(clk) then
			--reset is active high and will return the elevator to floor1
			--Question: is reset synchronous or asynchronous?
			if reset='1' then
				floor_state <= floor1;
			--now we will code our next-state logic
			else
				case floor_state is
					when floor1 =>
						if (up_down='1' and stop='0') then 
							floor_state <= floor2;
						else
							floor_state <= floor1;
						end if;
						
					when floor2 => 
						if (up_down='1' and stop='0') then 
							floor_state <= floor3;
						elsif (up_down='0' and stop='0') then 
							floor_state <= floor1;
						else
							floor_state <= floor2;
						end if;
					
					when floor3 =>
						if (up_down='1' and stop='0') then 
							floor_state <= floor4;
						elsif (up_down='0' and stop='0') then 
							floor_state <= floor2;
						else
							floor_state <= floor3;
						end if;
						
					when floor4 =>
						if (up_down='1' and stop='0') then 
							floor_state <= floor5;
						elsif (up_down='0' and stop='0') then 
							floor_state <= floor3;
						else
							floor_state <= floor4;
						end if;
						
					when floor5 =>
						if (up_down='1' and stop='0') then 
							floor_state <= floor6;
						elsif (up_down='0' and stop='0') then 
							floor_state <= floor4;
						else
							floor_state <= floor5;
						end if;
						
					when floor6 =>
						if (up_down='1' and stop='0') then 
							floor_state <= floor7;
						elsif (up_down='0' and stop='0') then 
							floor_state <= floor5;
						else
							floor_state <= floor6;
						end if;
					
					when floor7 =>
						if (up_down='1' and stop='0') then 
							floor_state <= floor8;
						elsif (up_down='0' and stop='0') then 
							floor_state <= floor6;
						else
							floor_state <= floor7;
						end if;
						
					when floor8 =>
						if (up_down='0' and stop='0') then 
							floor_state <= floor7;
						else 
							floor_state <= floor8;
						end if;
					
					--This line accounts for phantom states
					when others =>
						floor_state <= floor1;
						
				end case;
			end if;
		end if;
	end process;
	
	-- Here you define your output logic. Finish the statements below
	floor <= "0010" when (floor_state = floor1) else
				"0011" when (floor_state = floor2) else
				"0101" when (floor_state = floor3) else
				"0111" when (floor_state = floor4) else
				"0001" when (floor_state = floor5) else
				"0011" when (floor_state = floor6) else
				"0111" when (floor_state = floor7) else
				"1001" when (floor_state = floor8) else
				"0001";
				
	carry <= "0001" when (floor_state = floor5) else
				"0001" when (floor_state = floor6) else
				"0001" when (floor_state = floor7) else
				"0001" when (floor_state = floor8) else
				"0000";
				
	end Behavioral;

  

### Debugging
I encountered a few errors when generating the .bit file for the Moore Elevator Controller. After some hunting I found the issue to be in the .ucf file where I named some of the signals incorrectly. 

### Testing methodology or results
Part 1: Required functionality. The seven segment display starts at one. When the up/down button is held, meaning go up, the numbers increment by one. When the up/down button is released the numbers go down in the same manner. When the stop button is held. The floor stop where it is and doesn't change until the stop button is released. For this part the LEDs flash in sync with the clock transitions. When the Mealy machine was implemented it correctly shows the next floor state on another seven segment display.

Part 2: B Functionality: More Floors. After changing the shell to go up by prime numbers from 2 to 19, I tested the board again. When powered on the first floor is floor 2 shown on the display. When the up/down button is pressed the floor numbers go up by prime numbers, 2, 3, 5, etc. When the button is released the floor number goes down in the same manner. 
B Functionality: Change inputs. This time the inputs changed from two buttons to three switches that indicate the destination floor. On start up the floor number is zero as expected. When given a 3 bit input the elevator control goes to that floor number without skipping any floors in between.

Part 3: A Functionality: Moving Lights. The seven segment displays and elevator controls are the same as in the last functionality The added functionality here is an LED light that corresponds to the floor the elevator is on. Zero is the right most LED and seven is the left most. When a floor input was given both the seven segment display and LED would show the correct floor. When moving the LEDs also only moved left/up one at a time and right/down one at a time.
A Functionality: Multiple Elevators. This functionality went uncompleted. I got as far as having two elevators with separate inputs that both behaved as described above. However, I was not able to get the closest one to respond to a different call given by the user. 

### Observations and Conclusions
The purpose of this lab was to modify our existing shell code to implement various functions. All functions but the multiple elevators were fully implemented. I felt a little unprepared with some of the functions that would be needed to complete the multiple elevators process. Overall it was a good lab. I felt like I was able to figure it all out after some thought.

### Documentation
None