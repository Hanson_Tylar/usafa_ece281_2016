	--------------------------------------------------------------------
	-- Name: Tylar Hanson
	-- Date: 28 MAR 2016
	-- Course: ECE 281
	-- File: MooreElevatorController_shell.vhd
	-- HW: Lab #3 - Elevator Controller
	--
	-- Purp: Design an elevator controller with several variations 
	--
	-- Doc: None.
	--
	-- Academic Integrity Statement: I certify that, while others may have 
	-- assisted me in brain storming, debugging and validating this program, 
	-- the program itself is my own work. I understand that submitting code 
	-- which is the work of other individuals is a violation of the honor   
	-- code.  I also understand that if I knowingly give my original work to 
	-- another individual is also a violation of the honor code. 
	------------------------------------------------------------------------- 
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MooreElevatorController_Shell is
    Port ( clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           destination : in  STD_LOGIC_VECTOR (2 downto 0);
			  floor_light : out STD_LOGIC_VECTOR (7 downto 0);
           floor : out  STD_LOGIC_VECTOR (3 downto 0));
end MooreElevatorController_Shell;

architecture Behavioral of MooreElevatorController_Shell is

--Below you create a new variable type! You also define what values that 
--variable type can take on. Now you can assign a signal as 
--"floor_state_type" the same way you'd assign a signal as std_logic 
type floor_state_type is (floor0, floor1, floor2, floor3, floor4, floor5, floor6, floor7);

--Here you create a variable "floor_state" that can take on the values
--defined above. Neat-o!
signal floor_state : floor_state_type;

begin
---------------------------------------------
--Below you will code your next-state process
---------------------------------------------

--This line will set up a process that is sensitive to the clock
floor_state_machine: process(clk)
begin
	if rising_edge(clk) then
		if reset='1' then
			floor_state <= floor1;
		--now we will code our next-state logic
			else
			case floor_state is
			
				when floor0 =>
					if (destination > "000") then 
						floor_state <= floor1;
					else
						floor_state <= floor0;
					end if;
					
				when floor1 => 
					if (destination > "001") then 
						floor_state <= floor2;
					elsif (destination < "001") then 
						floor_state <= floor0;
					else
						floor_state <= floor1;
					end if;
					
				when floor2 => 
					if (destination > "010") then 
						floor_state <= floor3;
					elsif (destination < "010") then 
						floor_state <= floor1;
					else
						floor_state <= floor2;
					end if;
				
				when floor3 =>
					if (destination > "011") then 
						floor_state <= floor4;
					elsif (destination < "011") then 
						floor_state <= floor2;
					else
						floor_state <= floor3;
					end if;
					
				when floor4 =>
					if (destination > "100") then 
						floor_state <= floor5;
					elsif (destination < "100") then 
						floor_state <= floor3;
					else
						floor_state <= floor4;
					end if;
					
				when floor5 =>
					if (destination > "101") then 
						floor_state <= floor6;
					elsif (destination < "101") then 
						floor_state <= floor4;
					else
						floor_state <= floor5;
					end if;
					
				when floor6 =>
					if (destination > "110") then 
						floor_state <= floor7;
					elsif (destination < "110") then 
						floor_state <= floor5;
					else
						floor_state <= floor6;
					end if;
					
				when floor7 =>
					if (destination >= "111") then 
						floor_state <= floor7;
					else 
						floor_state <= floor6;
					end if;
				
				--This line accounts for phantom states
				when others =>
					floor_state <= floor1;
			end case;
		end if;
	end if;
end process;

-- Here you define your output logic. Finish the statements below
floor <= "0000" when (floor_state = floor0) else
			"0001" when (floor_state = floor1) else
			"0010" when (floor_state = floor2) else
			"0011" when (floor_state = floor3) else
			"0100" when (floor_state = floor4) else
			"0101" when (floor_state = floor5) else
			"0110" when (floor_state = floor6) else
			"0111" when (floor_state = floor7) else
			"0001";

floor_light <= "00000001" when (floor_state=floor0) else
					"00000010" when (floor_state=floor1) else
					"00000100" when (floor_state=floor2) else
					"00001000" when (floor_state=floor3) else
					"00010000" when (floor_state=floor4) else
					"00100000" when (floor_state=floor5) else
					"01000000" when (floor_state=floor6) else
					"10000000" when (floor_state=floor7) else 
					"00000000";
end Behavioral;

