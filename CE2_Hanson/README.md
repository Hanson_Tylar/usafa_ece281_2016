# Computer Exercise #2 - Implement Half-Adder

## By C3C Tylar Hanson

## Table of Contents
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Preliminary Design](#preliminary-design)
3. [Hardware schematic](#hardware-schematic)
4. [Well-formatted code](#well-formatted-code)
5. [Debugging](#debugging)
6. [Testing methodology or results](#testing-methodology-or-results)
7. [Answers to Lab Questions](#answers-to-lab-questions)
8. [Observations and Conclusions](#observations-and-conclusions)
9. [Documentation](#documentation)
 
### Objectives or Purpose 
In this computer exercise I will write, test, and implement a half-adder on the Nexys2 Development Board.  A half-adder takes two single-bit inputs and outputs their sum.

### Preliminary design

This is the truth table for a half-adder.

| A | B | C | S |
|:-:|:-:|:-:|:-:|
| 0 | 0 | 0 | 0 |
| 0 | 1 | 0 | 1 |
| 1 | 0 | 0 | 1 |
| 1 | 1 | 1 | 0 |

### Hardware schematic

![Half Adder Schematic](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/200f0c6f6d7c323d8ee61d7d0a11735749ac78ad/CE2_Hanson/CE2_Half_Adder_Schematic_Capture.JPG?token=f0ec9cd360e40fff81f0abccb53e5586ec979bac)

![Top Schematic](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/200f0c6f6d7c323d8ee61d7d0a11735749ac78ad/CE2_Hanson/CE2_Top_Schematic_Capture.JPG?token=bcd8f91c6d19a8b42ddb5381c3d6a47e48a43c33)

### Well-formatted code

    LIBRARY ieee;
	USE ieee.std_logic_1164.ALL;
	USE ieee.numeric_std.ALL;
	LIBRARY UNISIM;
	USE UNISIM.Vcomponents.ALL;
	ENTITY CE2_Hanson_HA_CE2_Hanson_HA_sch_tb IS
	END CE2_Hanson_HA_CE2_Hanson_HA_sch_tb;
	ARCHITECTURE behavioral OF CE2_Hanson_HA_CE2_Hanson_HA_sch_tb IS 

    COMPONENT CE2_Hanson_HA
    PORT( A	:	IN	STD_LOGIC; 
          B	:	IN	STD_LOGIC; 
          C_out	:	OUT	STD_LOGIC; 
          S	:	OUT	STD_LOGIC);
   	END COMPONENT;

  	 SIGNAL A	:	STD_LOGIC;
   	 SIGNAL B	:	STD_LOGIC;
  	 SIGNAL C_out	:	STD_LOGIC;
  	 SIGNAL S	:	STD_LOGIC;

	BEGIN

   	UUT: CE2_Hanson_HA PORT MAP(
		A => A, 
		B => B, 
		C_out => C_out, 
		S => S
   	);

	-- *** Test Bench - User Defined Section ***
   	tb : PROCESS
   	BEGIN
			A <= '0'; B <= '0';
			wait for 10 ns;
	
			A <= '0'; B <= '1';
			wait for 10 ns;
	
			A <= '1'; B <= '0';
			wait for 10 ns;
	
			A <= '1'; B <= '1';
			wait for 10 ns;
	
      WAIT; -- will wait forever
    END PROCESS;
     -- *** End Test Bench - User Defined Section ***

	END;

### Debugging

ERROR:HDLCompiler:69 - <sw1> is not declared.     
I realized that input A and B needed to be used in the user defined section, not sw1 and sw0.

I got to the final step and the LEDs aren't turning on for any switch combination. I realized here that I downloaded the .ucf file but didn't associate it with my project.

### Testing methodology or results

In this figure is the simulation of the circuit built with the Nexys2 board.
![ISIM Capture](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/f4edb1f99680330ea8e67c7d8a5efbde9f203b5f/CE2_Hanson/CE2_ISim_Capture.JPG?token=33134e952404bc47b0f1731095ae8bffa3742af9)

I know that this simulation is correct because when I add inputs A and B the sum is always shown by outputs c_out and s. For instance 1 + 1 = 10<sub>2</sub>

The following pictures demonstrate the circuit given four different inputs.

00![HA_00](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/f6eb795d76e7ebb6366d5b1b314f3bd72a171b7b/CE2_Hanson/HA_00.jpg?token=049b994c64e5b4a86b03cb4317f0f40e7d5fefd0)
01
![HA_01](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/f6eb795d76e7ebb6366d5b1b314f3bd72a171b7b/CE2_Hanson/HA_01.jpg?token=7fbf24a6f8981340f05b866b2cfa82365606932c)

10
![HA_10](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/f6eb795d76e7ebb6366d5b1b314f3bd72a171b7b/CE2_Hanson/HA_10.jpg?token=56f45dec89cc91a56fe30cef77a6141abc23f7cf)
11
![HA_11](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/f6eb795d76e7ebb6366d5b1b314f3bd72a171b7b/CE2_Hanson/HA_11.jpg?token=bfca12604a81bd04a50a4a0f24fb8ca0b9e6994d)

Again no matter the input the output is always the binary sum.

### Answers to Lab Questions

The purpose of the CE2 constraints file is this file tells Xilinx how the input/output ports map to actual FPGA pins.

### Observations and Conclusions
This exercise went a lot smoother than the last. I learned how to put actual images in my lab notebook and I ran into a lot fewer errors. The concept of a half-adder is fairly simple. I'm not quite sure why we needed two different schematics for this one. The only difference I can really see is that the top schematic shows switch inputs and hides the gates inside.

### Documentation
None