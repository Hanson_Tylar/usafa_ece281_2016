<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="sw1" />
        <signal name="sw0" />
        <signal name="led1" />
        <signal name="led0" />
        <port polarity="Input" name="sw1" />
        <port polarity="Input" name="sw0" />
        <port polarity="Output" name="led1" />
        <port polarity="Output" name="led0" />
        <blockdef name="CE2_Hanson_HA">
            <timestamp>2016-1-19T2:35:58</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <block symbolname="CE2_Hanson_HA" name="XLXI_2">
            <blockpin signalname="sw1" name="A" />
            <blockpin signalname="sw0" name="B" />
            <blockpin signalname="led1" name="C_out" />
            <blockpin signalname="led0" name="S" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="320" y="256" name="XLXI_2" orien="R0">
        </instance>
        <branch name="sw1">
            <wire x2="320" y1="160" y2="160" x1="288" />
        </branch>
        <iomarker fontsize="28" x="288" y="160" name="sw1" orien="R180" />
        <branch name="sw0">
            <wire x2="320" y1="224" y2="224" x1="288" />
        </branch>
        <iomarker fontsize="28" x="288" y="224" name="sw0" orien="R180" />
        <branch name="led1">
            <wire x2="736" y1="160" y2="160" x1="704" />
        </branch>
        <iomarker fontsize="28" x="736" y="160" name="led1" orien="R0" />
        <branch name="led0">
            <wire x2="736" y1="224" y2="224" x1="704" />
        </branch>
        <iomarker fontsize="28" x="736" y="224" name="led0" orien="R0" />
    </sheet>
</drawing>