# Lab 4 - MIPS Single-Cycle Processor 

## By C3C Tylar Hanson

## Table of Contents
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Preliminary Design](#preliminary-design)
3. [Well-formatted code](#well-formatted-code)
4. [Debugging](#debugging)
5. [Testing methodology or results](#testing-methodology-or-results)
6. [Answers to Lab Questions](#answers-to-lab-questions)
7. [Observations and Conclusions](#observations-and-conclusions)
8. [Documentation](#documentation)
 
### Objectives or Purpose 
In this lab I will build a simplified MIPS single-cycle processor using VHDL.  I will combine the ALU I previously created with the code for the rest of the processor. Then I will load a test program and confirm that the system works. Next, I will implement two new instructions, and then write a new test program that confirms the new instructions work as well. By the end of this lab, I should thoroughly understand the internal operation of the MIPS single-cycle processor.

### Preliminary design
To better understand what's going on in this processor I filled in a table of instructions and what I expected the signal values to be. I then compared them to the simulation wave forms after creating a test bench.

![](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/7fd2163b06413b43cd1423f0ba056ab46095abe8/Lab4_Hanson/table_1.JPG?token=e8bf2881aea960d4e6c29d227463bd467dcf49a4)

To begin adding more functionality to the processor I started with the schematic.

![](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/195235f49800e9f2e78862505c06c6f7067f58f3/Lab4_Hanson/modded_sch.png?token=c0e32ac69f712596a3c2287f620a009ff05bf135)

After modifying the schematic I inserted the expanded functionality into these tables to aid in editing the VHDL.

![](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/7aef35459ca9367b5f6e00d0203872ea07b3ce27/Lab4_Hanson/modded_logic_tables.JPG?token=e41742bfc7e4101c5f79c33215318f6473eadf79)

### Well-formatted code
Below is the VHDL for the mips_combo. To change between the sets of instructions just edit the file path on line 529.

    --------------------------------------------------------------------
	-- Name: Tylar Hanson
	-- Date: 28 March 2016
	-- Course:	ECE_281
	-- File: mips_combo.vhd
	-- HW:	Lab #4 MIPS Single-Cylce Processor
	--
	-- Purp: Produce the appropriate outpute on the seven-segment display
	-- given an input character to display.
	--
	-- Doc:	None
	--
	-- Academic Integrity Statement: I certify that, while others may have 
	-- assisted me in brain storming, debugging and validating this program, 
	-- the program itself is my own work. I understand that submitting code 
	-- which is the work of other individuals is a violation of the honor   
	-- code.  I also understand that if I knowingly give my original work to 
	-- another individual is also a violation of the honor code. 
	------------------------------------------------------------------------- 
	
	------------------------------------------------------------
	-- Entity Declarations
	------------------------------------------------------------
	
	library IEEE; use IEEE.STD_LOGIC_1164.all;
	entity mips is -- single cycle MIPS processor
	  port(clk, reset:        in  STD_LOGIC;
	       pc:                inout STD_LOGIC_VECTOR(31 downto 0);
	       instr:             in  STD_LOGIC_VECTOR(31 downto 0);
	       memwrite:          out STD_LOGIC;
	       aluout, writedata: inout STD_LOGIC_VECTOR(31 downto 0);
	       readdata:          in  STD_LOGIC_VECTOR(31 downto 0));
	end;
	
	library IEEE; use IEEE.STD_LOGIC_1164.all;
	entity controller is -- single cycle control decoder
	  port(op, funct:          in  STD_LOGIC_VECTOR(5 downto 0);
	       zero:               in  STD_LOGIC;
	       memtoreg, memwrite: out STD_LOGIC;
	       pcsrc: 					out STD_LOGIC;
			 -- Adding ORI Capability
			 alusrc:      			out STD_LOGIC_VECTOR(1 downto 0);
	       regdst, regwrite:   out STD_LOGIC;
	       jump:               out STD_LOGIC;
	       alucontrol:         out STD_LOGIC_VECTOR(2 downto 0));
	end;
	
	library IEEE; use IEEE.STD_LOGIC_1164.all;
	entity maindec is -- main control decoder
	  port(op:                 in  STD_LOGIC_VECTOR(5 downto 0);
	       memtoreg, memwrite: out STD_LOGIC;
	       branch:     			out STD_LOGIC;
			 -- Adding BNE Capability
			 bne:     				out STD_LOGIC;
			 -- Adding ORI Capability
			 alusrc:      			out STD_LOGIC_VECTOR(1 downto 0);
	       regdst, regwrite:   out STD_LOGIC;
	       jump:               out STD_LOGIC;
	       aluop:              out  STD_LOGIC_VECTOR(1 downto 0));
	end;
	
	library IEEE; use IEEE.STD_LOGIC_1164.all;
	entity aludec is -- ALU control decoder
	  port(funct:      in  STD_LOGIC_VECTOR(5 downto 0);
	       aluop:      in  STD_LOGIC_VECTOR(1 downto 0);
	       alucontrol: out STD_LOGIC_VECTOR(2 downto 0));
	end;
	
	library IEEE; use IEEE.STD_LOGIC_1164.all; use IEEE.STD_LOGIC_ARITH.all;
	entity datapath is  -- MIPS datapath
	  port(clk, reset:        in  STD_LOGIC;
	       memtoreg, pcsrc:   in  STD_LOGIC;
			 -- Adding ORI Capability
			 alusrc:      			in STD_LOGIC_VECTOR(1 downto 0); 
	       regdst:    			in  STD_LOGIC;
	       regwrite, jump:    in  STD_LOGIC;
	       alucontrol:        in  STD_LOGIC_VECTOR(2 downto 0);
	       zero:              out STD_LOGIC;
	       pc:                inout STD_LOGIC_VECTOR(31 downto 0);
	       instr:             in  STD_LOGIC_VECTOR(31 downto 0);
	       aluout, writedata: inout STD_LOGIC_VECTOR(31 downto 0);
	       readdata:          in  STD_LOGIC_VECTOR(31 downto 0));
	end;
	
	--your ALU entity declaration goes here.  In this file, f is called alucontrol and y is
	--			called result and is an inout.
	
	library IEEE;
	use IEEE.STD_LOGIC_1164.ALL;
	use ieee.std_logic_signed.all;
	use IEEE.NUMERIC_STD.ALL;
	
	entity alu is
	    Port ( a, b : in  STD_LOGIC_VECTOR (31 downto 0);
	           alucontrol : in  STD_LOGIC_VECTOR (2 downto 0);
	           result : inout  STD_LOGIC_VECTOR (31 downto 0);
	           zero : out  STD_LOGIC);
	end alu;
	
	library IEEE; use IEEE.STD_LOGIC_1164.all; 
	use IEEE.STD_LOGIC_UNSIGNED.all;
	entity regfile is -- three-port register file
	  port(clk:           in  STD_LOGIC;
	       we3:           in  STD_LOGIC;
	       ra1, ra2, wa3: in  STD_LOGIC_VECTOR(4 downto 0);
	       wd3:           in  STD_LOGIC_VECTOR(31 downto 0);
	       rd1, rd2:      out STD_LOGIC_VECTOR(31 downto 0));
	end;
	
	library IEEE; use IEEE.STD_LOGIC_1164.all; 
	use IEEE.STD_LOGIC_UNSIGNED.all;
	entity adder is -- adder
	  port(a, b: in  STD_LOGIC_VECTOR(31 downto 0);
	       y:    out STD_LOGIC_VECTOR(31 downto 0));
	end;
	
	library IEEE; use IEEE.STD_LOGIC_1164.all;
	entity sl2 is -- shift left by 2
	  port(a: in  STD_LOGIC_VECTOR(31 downto 0);
	       y: out STD_LOGIC_VECTOR(31 downto 0));
	end;
	
	library IEEE; use IEEE.STD_LOGIC_1164.all;
	entity signext is -- sign extender
	  port(a: in  STD_LOGIC_VECTOR(15 downto 0);
	       y: out STD_LOGIC_VECTOR(31 downto 0));
	end;
	
	library IEEE; use IEEE.STD_LOGIC_1164.all;
	entity zeroext is -- zero extender
	  port(a: in  STD_LOGIC_VECTOR(15 downto 0);
	       y: out STD_LOGIC_VECTOR(31 downto 0));
	end;
	
	library IEEE; use IEEE.STD_LOGIC_1164.all;  use IEEE.STD_LOGIC_ARITH.all;
	entity flopr is -- flip-flop with synchronous reset
	  generic(width: integer);
	  port(clk, reset: in  STD_LOGIC;
	       d:          in  STD_LOGIC_VECTOR(width-1 downto 0);
	       q:          out STD_LOGIC_VECTOR(width-1 downto 0));
	end;
	
	library IEEE; use IEEE.STD_LOGIC_1164.all;
	entity mux2 is -- two-input multiplexer
	  generic(width: integer);
	  port(d0, d1: in  STD_LOGIC_VECTOR(width-1 downto 0);
	       s:      in  STD_LOGIC;
	       y:      out STD_LOGIC_VECTOR(width-1 downto 0));
	end;
	
	library IEEE; use IEEE.STD_LOGIC_1164.all;
	entity mux3 is -- three-input multiplexer
	  generic(width: integer);
	  port(d0, d1, d2: in  STD_LOGIC_VECTOR(width-1 downto 0);
	       s:      in  STD_LOGIC_VECTOR(1 downto 0);
	       y:      out STD_LOGIC_VECTOR(width-1 downto 0));
	end;
	
	library IEEE; 
	use IEEE.STD_LOGIC_1164.all; 
	--use IEEE.NUMERIC_STD_UNSIGNED.all;
	use IEEE.NUMERIC_STD.all;
	
	entity top is -- top-level design for testing
	  port(clk, reset:           in     STD_LOGIC;
	       writedata, dataadr:   inout STD_LOGIC_VECTOR(31 downto 0);
	       memwrite:             inout STD_LOGIC);
	end;
	
	library IEEE; 
	use IEEE.STD_LOGIC_1164.all; use STD.TEXTIO.all;
	--use IEEE.NUMERIC_STD_UNSIGNED.all; 
	use IEEE.NUMERIC_STD.all; 
	
	entity dmem is -- data memory
	  port(clk, we:  in STD_LOGIC;
	       a, wd:    in STD_LOGIC_VECTOR(31 downto 0);
	       rd:       out STD_LOGIC_VECTOR(31 downto 0));
	end;
	
	library IEEE; 
	use IEEE.STD_LOGIC_1164.all; 
	use STD.TEXTIO.all;
	--use IEEE.NUMERIC_STD_UNSIGNED.all;  
	use IEEE.NUMERIC_STD.all; 
	use IEEE.STD_LOGIC_UNSIGNED.all;  use IEEE.STD_LOGIC_ARITH.all;
	
	
	entity imem is -- instruction memory
	  port(a:  in  STD_LOGIC_VECTOR(5 downto 0);
	       rd: out STD_LOGIC_VECTOR(31 downto 0));
	end;
	
	---------------------------------------------------------
	-- Architecture Definitions
	---------------------------------------------------------
	
	architecture struct of mips is
	  component controller
	    port(op, funct:          in  STD_LOGIC_VECTOR(5 downto 0);
	         zero:               in  STD_LOGIC;
	         memtoreg, memwrite: out STD_LOGIC;
	         pcsrc:			      out STD_LOGIC;
				-- Adding ORI Capability
				alusrc:      			out STD_LOGIC_VECTOR(1 downto 0);
	         regdst, regwrite:   out STD_LOGIC;
	         jump:               out STD_LOGIC;
	         alucontrol:         out STD_LOGIC_VECTOR(2 downto 0));
	  end component;
	  component datapath
	    port(clk, reset:        in  STD_LOGIC;
	         memtoreg, pcsrc:   in  STD_LOGIC;
				-- Adding ORI Capability
				alusrc:      			in STD_LOGIC_VECTOR(1 downto 0);
	         regdst:   			 in  STD_LOGIC;
	         regwrite, jump:    in  STD_LOGIC;
	         alucontrol:        in  STD_LOGIC_VECTOR(2 downto 0);
	         zero:              out STD_LOGIC;
	         pc:                inout STD_LOGIC_VECTOR(31 downto 0);
	         instr:             in STD_LOGIC_VECTOR(31 downto 0);
	         aluout, writedata: inout STD_LOGIC_VECTOR(31 downto 0);
	         readdata:          in  STD_LOGIC_VECTOR(31 downto 0));
	  end component;
	  signal memtoreg, regdst, regwrite, jump, pcsrc: STD_LOGIC;
		-- Adding ORI Capability
	  signal alusrc: std_logic_vector(1 downto 0);  
	  signal zero: STD_LOGIC;
	  signal alucontrol: STD_LOGIC_VECTOR(2 downto 0);
	begin
	  cont: controller port map(instr(31 downto 26), instr(5 downto 0),
	                            zero, memtoreg, memwrite, pcsrc, alusrc,
										 regdst, regwrite, jump, alucontrol);
	  dp: datapath port map(clk, reset, memtoreg, pcsrc, alusrc, regdst,
	                        regwrite, jump, alucontrol, zero, pc, instr,
									aluout, writedata, readdata);
	end;
	
	architecture struct of controller is
	  component maindec
	    port(op:                 in  STD_LOGIC_VECTOR(5 downto 0);
	         memtoreg, memwrite: out STD_LOGIC;
	         branch:     			out STD_LOGIC;
				-- Adding BNE Capability
				bne:						out STD_LOGIC;
				-- Adding ORI Capability
				alusrc:      			out STD_LOGIC_VECTOR(1 downto 0);
	         regdst, regwrite:   out STD_LOGIC;
	         jump:               out STD_LOGIC;
	         aluop:              out  STD_LOGIC_VECTOR(1 downto 0));
	  end component;
	  component aludec
	    port(funct:      in  STD_LOGIC_VECTOR(5 downto 0);
	         aluop:      in  STD_LOGIC_VECTOR(1 downto 0);
	         alucontrol: out STD_LOGIC_VECTOR(2 downto 0));
	  end component;
	  signal aluop: STD_LOGIC_VECTOR(1 downto 0);
	  signal branch: STD_LOGIC;
	  -- Adding BNE Capability
	  signal bne: STD_LOGIC;
	  
	begin
		-- Add BNE
	  md: maindec port map(op, memtoreg, memwrite, branch, bne,
	                       alusrc, regdst, regwrite, jump, aluop);
	  ad: aludec port map(funct, aluop, alucontrol);
		-- Change PCSrc signal to include BNE
	  pcsrc <= (branch and zero) or (bne and (not zero));
	
	end;
	
	architecture behave of maindec is
	  signal controls: STD_LOGIC_VECTOR(10 downto 0);
	begin
	  process(op) begin
	    case op is							
	      when "000000" => controls <= "01100000010"; -- Rtype
	      when "100011" => controls <= "01001001000"; -- LW
	      when "101011" => controls <= "00001010000"; -- SW
	      when "000100" => controls <= "00000100001"; -- BEQ
	      when "001000" => controls <= "01001000000"; -- ADDI
	      when "000010" => controls <= "00000000100"; -- J
			when "001101" => controls <= "01010000011"; -- ORI
			when "000101" => controls <= "10000000001"; -- BNE
	      when others   => controls <= "-----------"; -- illegal op
	    end case;
	  end process;
	  bne 	  <= controls(10);
	  regwrite <= controls(9);
	  regdst   <= controls(8);
	  alusrc   <= controls(7 downto 6);  
	  branch   <= controls(5);
	  memwrite <= controls(4);
	  memtoreg <= controls(3);
	  jump     <= controls(2);
	  aluop    <= controls(1 downto 0);
	end;
	
	architecture behave of aludec is
	begin
	  process(aluop, funct) begin
	    case aluop is
	      when "00" => alucontrol <= "010"; -- add (for lb/sb/addi)
	      when "01" => alucontrol <= "110"; -- sub (for beq)
			-- Added ORI capability
			when "11" => alucontrol <= "001"; -- or (for ORI)
	      when others => case funct is         -- R-type instructions
	                         when "100000" => alucontrol <= "010"; -- add (for add)
	                         when "100010" => alucontrol <= "110"; -- subtract (for sub)
	                         when "100100" => alucontrol <= "000"; -- logical and (for and)
	                         when "100101" => alucontrol <= "001"; -- logical or (for or)
	                         when "101010" => alucontrol <= "111"; -- set on less (for slt)
	                         when others   => alucontrol <= "---"; -- should never happen
	                     end case;
	    end case;
	  end process;
	end;
	
	architecture struct of datapath is
	  component alu
	    port(a, b:       in  STD_LOGIC_VECTOR(31 downto 0);
	         alucontrol: in  STD_LOGIC_VECTOR(2 downto 0);
	         result:     inout STD_LOGIC_VECTOR(31 downto 0);
		      zero:       out STD_LOGIC);
	  end component;
	  component regfile
	    port(clk:           in  STD_LOGIC;
	         we3:           in  STD_LOGIC;
	         ra1, ra2, wa3: in  STD_LOGIC_VECTOR(4 downto 0);
	         wd3:           in  STD_LOGIC_VECTOR(31 downto 0);
	         rd1, rd2:      out STD_LOGIC_VECTOR(31 downto 0));
	  end component;
	  component adder
	    port(a, b: in  STD_LOGIC_VECTOR(31 downto 0);
	         y:    out STD_LOGIC_VECTOR(31 downto 0));
	  end component;
	  component sl2
	    port(a: in  STD_LOGIC_VECTOR(31 downto 0);
	         y: out STD_LOGIC_VECTOR(31 downto 0));
	  end component;
	  component signext
	    port(a: in  STD_LOGIC_VECTOR(15 downto 0);
	         y: out STD_LOGIC_VECTOR(31 downto 0));
	  end component;
		component zeroext
			port(a: in  STD_LOGIC_VECTOR(15 downto 0);
	         y: out STD_LOGIC_VECTOR(31 downto 0));
	  end component;
	  component flopr generic(width: integer);
	    port(clk, reset: in  STD_LOGIC;
	         d:          in  STD_LOGIC_VECTOR(width-1 downto 0);
	         q:          out STD_LOGIC_VECTOR(width-1 downto 0));
	  end component;
	  component mux2 generic(width: integer);
	    port(d0, d1: in  STD_LOGIC_VECTOR(width-1 downto 0);
	         s:      in  STD_LOGIC;
	         y:      out STD_LOGIC_VECTOR(width-1 downto 0));
	  end component;
	  -- Added for ORI Capability
	  component mux3 generic(width: integer);
	    port(d0, d1, d2: in  STD_LOGIC_VECTOR(width-1 downto 0);
	         s:      in  STD_LOGIC_VECTOR(1 downto 0);
	         y:      out STD_LOGIC_VECTOR(width-1 downto 0));
	  end component;
	  signal writereg: STD_LOGIC_VECTOR(4 downto 0);
	  signal pcjump, pcnext, pcnextbr, pcplus4, pcbranch: STD_LOGIC_VECTOR(31 downto 0);
	  -- Added zeroimm for ORI capability
	  signal signimm, signimmsh, zeroimm: STD_LOGIC_VECTOR(31 downto 0); 
	  signal srca, srcb, result: STD_LOGIC_VECTOR(31 downto 0);
	begin
	  -- next PC logic
	  pcjump <= pcplus4(31 downto 28) & instr(25 downto 0) & "00";
	  pcreg: flopr generic map(32) port map(clk, reset, pcnext, pc);
	  pcadd1: adder port map(pc, X"00000004", pcplus4);
	  immsh: sl2 port map(signimm, signimmsh);
	  pcadd2: adder port map(pcplus4, signimmsh, pcbranch);
	  pcbrmux: mux2 generic map(32) port map(pcplus4, pcbranch, pcsrc, pcnextbr);
	  pcmux: mux2 generic map(32) port map(pcnextbr, pcjump, jump, pcnext);
	
	  -- register file logic
	  rf: regfile port map(clk, regwrite, instr(25 downto 21), instr(20 downto 16),
	              writereg, result, srca, writedata);
	  wrmux: mux2 generic map(5) port map(instr(20 downto 16), instr(15 downto 11),
	                                      regdst, writereg);
	  resmux: mux2 generic map(32) port map(aluout, readdata, memtoreg, result);
	  se: signext port map(instr(15 downto 0), signimm);
	  ze: zeroext port map(instr(15 downto 0), zeroimm);
	
	  -- ALU logic
	  srcbmux: mux3 generic map(32) port map(writedata, signimm, zeroimm, alusrc, srcb); 
	  mainalu:  alu port map(srca, srcb, alucontrol, aluout, zero);
	end;
	
	--insert your ALU architecture here.
	
	architecture Behavioral of alu is
	
	signal temp : STD_LOGIC_VECTOR (31 downto 0);
	
	begin
	
		temp <= (a and b) when alucontrol = "000" else 
				  (a or b) when alucontrol = "001" else
				  (a + b) when alucontrol = "010" else
				  (a and (not b)) when alucontrol = "100" else
				  (a or (not b)) when alucontrol = "101" else
				  (a - b) when alucontrol = "110" else
				  x"00000001" when ( a < b ) and  alucontrol = "111" else
				  x"00000000";
		zero <= '1' when ( temp = x"00000000" ) else
				  '0';
		result <= temp;
	
	end Behavioral;
	
	architecture behave of regfile is
	  type ramtype is array (31 downto 0) of STD_LOGIC_VECTOR(31 downto 0);
	  signal mem: ramtype;
	begin
	  -- three-ported register file
	  -- read two ports combinationally
	  -- write third port on rising edge of clock
	  process(clk) begin
	    if clk'event and clk = '1' then
	       if we3 = '1' then mem(CONV_INTEGER(wa3)) <= wd3;
	       end if;
	    end if;
	  end process;
	  process(ra1, ra2, mem) begin
	    if (conv_integer(ra1) = 0) then rd1 <= X"00000000"; -- register 0 holds 0
	    else rd1 <= mem(CONV_INTEGER(ra1));
	    end if;
	    if (conv_integer(ra2) = 0) then rd2 <= X"00000000"; 
	    else rd2 <= mem(CONV_INTEGER(ra2));
	    end if;
	  end process;
	end;
	
	architecture behave of adder is
	begin
	  y <= a + b;
	end;
	
	architecture behave of sl2 is
	begin
	  y <= a(29 downto 0) & "00";
	end;
	
	architecture behave of signext is
	begin
	  y <= X"0000" & a when a(15) = '0' else X"ffff" & a; 
	end;
	
	architecture behave of zeroext is
	begin
	  y <= X"0000" & a; 
	end;
	
	architecture asynchronous of flopr is
	begin
	  process(clk, reset) begin
	    if reset = '1' then  q <= CONV_STD_LOGIC_VECTOR(0, width); 
	    elsif clk'event and clk = '1' then
	      q <= d;
	    end if;
	  end process;
	end;
	
	architecture behave of mux2 is
	begin
	  y <= d0 when s = '0' else d1;
	end;
	
	architecture behave of mux3 is
	begin
	  y <= d0 when s = "00" else
			 d1 when s = "01" else d2;
	end;
	
	 architecture test of top is
	  component mips 
	    port(clk, reset:        in  STD_LOGIC;
	         pc:                inout STD_LOGIC_VECTOR(31 downto 0);
	         instr:             in  STD_LOGIC_VECTOR(31 downto 0); 
	         memwrite:          out STD_LOGIC;
	         aluout, writedata: inout STD_LOGIC_VECTOR(31 downto 0);
	         readdata:          in  STD_LOGIC_VECTOR(31 downto 0));
	  end component;
	  component imem
	    port(a:  in  STD_LOGIC_VECTOR(5 downto 0);
	         rd: out STD_LOGIC_VECTOR(31 downto 0));
	  end component;
	  component dmem
	    port(clk, we:  in STD_LOGIC;
	         a, wd:    in STD_LOGIC_VECTOR(31 downto 0);
	         rd:       out STD_LOGIC_VECTOR(31 downto 0));
	  end component;
	  signal pc, instrLocal, 
	         readdata: STD_LOGIC_VECTOR(31 downto 0);
	begin
	  -- instantiate processor and memories
	  mips1: mips port map(clk, reset, pc, instrLocal, memwrite, dataadr, 
	                       writedata, readdata);
	  imem1: imem port map(pc(7 downto 2), instrLocal);
	  dmem1: dmem port map(clk, memwrite, dataadr, writedata, readdata);
	end;
	
	architecture behave of dmem is
	begin
	  process (clk, a)is
	    type ramtype is array (63 downto 0) of STD_LOGIC_VECTOR(31 downto 0);
	    variable mem: ramtype;
	  begin
	   
	      if clk'event and clk = '1' then
					
	          if (we = '1') then mem(to_integer(unsigned(a(7 downto 2)))) := wd;
	          end if;
	      end if;
	      rd <= mem(to_integer(unsigned(a(7 downto 2)))); 
	
	
	  end process;
	end;
	
	architecture behave of imem is
	begin
	    process is
	
	-- You'll need to create a path to your own file here.
			 file mem_file: text open read_mode is "C:\Users\C18Tylar.Hanson\Documents\Spring_2016\ECE_281\Xilinx_Projects\Lab4_Hanson\memfile2.dat";
	        variable L: line;
	        variable ch: character;
	        variable index, result: integer;
	        type ramtype is array (63 downto 0) of STD_LOGIC_VECTOR(31 downto 0);
	        variable mem: ramtype;
	    begin
	        -- initialize memory from file
		-- memory in little-endian format
		--  80020044 means mem[3] = 80 and mem[0] = 44
	        for i in 0 to 63 loop -- set all contents low
	            mem(conv_integer(i)) := X"00000000";
	        end loop;
	        index := 0; 
	        while not endfile(mem_file) loop
	            readline(mem_file, L);
	           
	                result := 0;
	                    for k in 1 to 8 loop
	                        read(L, ch);
	                        if '0' <= ch and ch <= '9' then 
	                            result := result*16 + character'pos(ch)-character'pos('0');
										
	                        elsif 'a' <= ch and ch <= 'f' then
	                            result := result*16 + character'pos(ch)-character'pos('a')+10;
										
	                        else report "Format error on line " & integer'image(index)
	                             severity error;
	                        end if;
	                    end loop;
	
	
	        
	            mem(index) := conv_std_logic_vector(result, 32);  
	            index := index + 1;
	        end loop;
	        
	        loop
	
	            rd <= mem(conv_integer(a));
	            wait on a;
	        end loop;
	    end process;
	end;


This is the code used to test both sets of instructions for the MIPS processor.

    --------------------------------------------------------------------
	-- Name: Tylar Hanson
	-- Date: 26 March 2016
	-- Course:	ECE_281
	-- File: mips_combo.vhd
	-- HW:	Lab #4 MIPS Single-Cylce Processor
	--
	-- Purp: Produce the appropriate outpute on the seven-segment display
	-- given an input character to display.
	--
	-- Doc:	None
	--
	-- Academic Integrity Statement: I certify that, while others may have 
	-- assisted me in brain storming, debugging and validating this program, 
	-- the program itself is my own work. I understand that submitting code 
	-- which is the work of other individuals is a violation of the honor   
	-- code.  I also understand that if I knowingly give my original work to 
	-- another individual is also a violation of the honor code. 
	------------------------------------------------------------------------- 
	LIBRARY ieee;
	USE ieee.std_logic_1164.ALL;
	 
	-- Uncomment the following library declaration if using
	-- arithmetic functions with Signed or Unsigned values
	--USE ieee.numeric_std.ALL;
	 
	ENTITY mips_combp_tb IS
	END mips_combp_tb;
	 
	ARCHITECTURE behavior OF mips_combp_tb IS 
	 
	    -- Component Declaration for the Unit Under Test (UUT)
	 
	    COMPONENT top
	    PORT(
	         clk : IN  std_logic;
	         reset : IN  std_logic;
	         writedata : INOUT  std_logic_vector(31 downto 0);
	         dataadr : INOUT  std_logic_vector(31 downto 0);
	         memwrite : INOUT  std_logic
	        );
	    END COMPONENT;
	    
	
	   --Inputs
	   signal clk : std_logic := '0';
	   signal reset : std_logic := '1';
	
		--BiDirs
	   signal writedata : std_logic_vector(31 downto 0);
	   signal dataadr : std_logic_vector(31 downto 0);
	   signal memwrite : std_logic;
	
	   -- Clock period definitions
	   constant clk_period : time := 10 ns;
	 
	BEGIN
	 
		-- Instantiate the Unit Under Test (UUT)
	   uut: top PORT MAP (
	          clk => clk,
	          reset => reset,
	          writedata => writedata,
	          dataadr => dataadr,
	          memwrite => memwrite
	        );
	
	   -- Clock process definitions
	   clk_process :process
	   begin
			clk <= '0';
			wait for clk_period/2;
			clk <= '1';
			wait for clk_period/2;
	   end process;
	 
	
	   -- Stimulus process
	   stim_proc: process
	   begin		
	      wait for clk_period;
	      -- insert stimulus here 
			reset <= '0'; 
			wait for clk_period;
			
	
	      wait;
	   end process;
	
	END;


### Debugging
After making the required modifications I encountered many errors due to typos and invalid syntax. I resolved this by starting at the top of the error list, resolving one error, checking again, and moving down the list. 
After fixing those errors and looking at the simulation waveform I noticed that the wrong value was being stored into memory at the end of the instruction list. Looking back through my .vhd file I realized I forgot to add my ALU entity and architecture statement. 

I ran into a problem where on the jump instruction on part two the ALU result was all X's. In the lab document it said to look out for the MARS compiler creating the wrong instruction. I wrote the jump instruction by hand and replaced it in memfile2.dat. After that the X's on ALU result went away. 

The final value for store word was one instead of two. I realized that I was using an unsigned library statement and after changing it to a signed library the final store word value was correct. 

### Testing methodology or results
Before implementing the added instructions I needed to add in my ALU from a previous computer exercise and make sure everything was operating as it should. The assembly code used in memfile.dat was created by converting instructions in Mars4.5. Also by using this program I know that the final store word value should be seven. It is seen in these results that under instruction twelve when memwrite goes high that the value stored is seven.  

![](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/db6424c1d8cc4db12c6fb2575ca0c1fbd5f723e4/Lab4_Hanson/premod_wavesim.JPG?token=8dac59e6d72676a9fea3c27a7776081f8811f5fc)

The zeroth instruction can be seen in the below picture on line three. This instruction says to put the value five into register two. This is reflected on the wavesim because aluout has the value 0x5 as does write data meaning the correct value was stored. The eighth instruction above is shown on line eleven below. Here the values in two registers are subtracted and should be equal, the aluout will be zero. The wavesim shows the values expected. This process was repeated for every instruction ensuring the processor is working appropriately.
 
![](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/b03ea63cda13acba5bd7c071c9108dd42afd8cf2/Lab4_Hanson/premod_assembly.JPG?token=02743c0071381efddf1f0edbdf1be39315faf759)

To have ORI capability I added a zero extender, changed the alusrc signal to two bits, and changed the mux choosing between a register and a signed immediate to a three input mux including the zero extended immediate .

To have capability to do a branch if not equal function, BNE, I first added a BNE signal coming from the main control decoder. This signal will be a one when the BNE instruction is given. The program counter source will then be changed to the new address if BNE is one and the two values are not equal, or if the Branch signal is one and the two values are equal. BNE and Branch are never expected to be one at the same time. This is the new set of instructions used to test proper functionality after adding hardware. 

![](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/9c9d4a4843d3c97b2b81a56299c23af8b74ac3cd/Lab4_Hanson/postmod_assembly.JPG?token=2c9529bae74ead410ecad72f12688cda19111cc2)

These are the results of the second set of instructions show above. Running the program in MIPS gives a final store word value of two, seen in instruction ten in the wavesim results. Using the same method as above I will explain a few of the new instructions. Instruction zero, also on line five above, ORs zero with the immediate value, 0x800. The result, 0x800, should be stored in a register. You can see that aluout and writedata matches this. Memwrite is also zero, so I know that this value is not being stored in memory. The fifth instruction, on line ten above, is a bne instruction. The values stored in the two registers are not equal so when the alu subtracts them the result should not be equal. The aluout in this case is one, what we expected. 

![](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/9c9d4a4843d3c97b2b81a56299c23af8b74ac3cd/Lab4_Hanson/postmod_wavesim.JPG?token=c4b6b75cfa6b298efee9301e55400f81a145db23)

### Answers to Lab Questions
The first program stored the value 0x7 to address 0x84.
The second program stores the value 0x2 to the address 0xffff7f54.

### Observations and Conclusions
I learned that using the wrong library can ruin many things. Overall, a good lab. It's crazy to think of everything that goes on behind the scenes of such simple instructions.

### Documentation
None