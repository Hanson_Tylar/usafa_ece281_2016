--------------------------------------------------------------------
-- Name: Tylar Hanson
-- Date: 09 April 2016
-- Course:	ECE_281
-- File: nibble_to_sseg.vhd
-- HW:	CE #4 32-Bit ALU
--
-- Purp: Design, write, and test a 32-bit Arithmetic Logic Unit (ALU)
--
-- Doc:	None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_signed.all;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity alu is
    Port ( a, b : in  STD_LOGIC_VECTOR (31 downto 0);
           alucontrol : in  STD_LOGIC_VECTOR (2 downto 0);
           result : out  STD_LOGIC_VECTOR (31 downto 0);
           zero : out  STD_LOGIC);
end alu;

architecture Behavioral of alu is

signal temp : STD_LOGIC_VECTOR (31 downto 0);

begin

	temp <= (a and b) when alucontrol = "000" else 
			  (a or b) when alucontrol = "001" else
			  (a + b) when alucontrol = "010" else
			  (a and (not b)) when alucontrol = "100" else
			  (a or (not b)) when alucontrol = "101" else
			  (a - b) when alucontrol = "110" else
			  x"00000001" when ( a < b ) and  alucontrol = "111" else
			  x"00000000";
	zero <= '1' when ( temp = x"00000000" ) else
			  '0';
	result <= temp;

end Behavioral;