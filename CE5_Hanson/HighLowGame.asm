#--------------------------------------------------------------------
#-- Name: Tylar Hanson
#-- Date: 20 April 2016
#-- Course:	ECE_281
#-- File: HighLowGame.asm
#-- HW:	CE #5
#--
#-- Purp: High-Low Guessing game
#--
#-- Doc: None
#--
#-- Academic Integrity Statement: I certify that, while others may have 
#-- assisted me in brain storming, debugging and validating this program, 
#-- the program itself is my own work. I understand that submitting code 
#-- which is the work of other individuals is a violation of the honor   
#-- code.  I also understand that if I knowingly give my original work to 
#-- another individual is also a violation of the honor code. 
#-------------------------------------------------------------------------

.text
main :
	# Put answer, 7, into register $s0
	addi $s0, $0, 0x37 # 7 in hex is 0x37
	
	# Put numbers in registers to be used later
	addi $s1, $0, 1
	lui $s3, 0xFFFF
	
	# Put the ascii values of letters in registers to be used later
	addi $s4, $0, 0x57 # Store W character
	addi $s5, $0, 0x48 # Store H character
	addi $s6, $9, 0x4C # Store L character
	
loop:
	# wait for user input
	lw $t0, 0($s3) # will be one if there is user input, zero otherwise
	beq $t0, $0, loop # Go back to loop if no user input
	lw $t1, 4($s3) # Else load the input to $t1
	
	# High, low, equal calculation 
	beq $s0, $t1, winner
	slt $t2, $t1, $s0   # $t2 = 1 if guess lower than answer
	beq $t2, $s1, toolow # if guess is too low go to toolow
	j toohigh # Guess is not correct and not less than, so it must be greater than
	
toolow:	
	# Output the text
	sw $s6, 0xC($s3)
	j loop
	
toohigh:	
	# Output the text
	sw $s5, 0xC($s3)
	j loop
	
winner:	
	# Output the text
	sw $s4, 0xC($s3)
	j end

end:	
	# Exit the program
	addi $v0, $0, 10
	syscall
