# Computer Exercise #5 - MIPS High-Low Game

## By C3C Tylar Hanson

## Table of Contents
1. [Objectives or Purpose](#objectives-or-purpose)
3. [Software flow chart or algorithms](#software-flow-chart-or-algorithms)
5. [Well-formatted code](#well-formatted-code)
6. [Debugging](#debugging)
7. [Testing methodology or results](#testing-methodology-or-results)
8. [Answers to Lab Questions](#answers-to-lab-questions)
9. [Observations and Conclusions](#observations-and-conclusions)
10. [Documentation](#documentation)
 
### Objectives or Purpose 
In this computer exercise, I will design, write, and test a high-low guessing game using a lightweight MIPS simulator.

### Software flow chart or algorithms
The flowchart below is my thought process for designing this game. 

![](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/23344ccc4a6aeef50849611b451e0d9f3d8efc6f/CE5_Hanson/pseudocode_flowchart.jpg?token=16da667170bcb8a0199c35acc46d5b3ffce9e1de)

### Well-formatted code
	--------------------------------------------------------------------
	-- Name: Tylar Hanson
	-- Date: 20 April 2016
	-- Course:	ECE_281
	-- File: HighLowGame.asm
	-- HW:	CE #5
	--
	-- Purp: High-Low Guessing game
	--
	-- Doc: None
	--
	-- Academic Integrity Statement: I certify that, while others may have 
	-- assisted me in brain storming, debugging and validating this program, 
	-- the program itself is my own work. I understand that submitting code 
	-- which is the work of other individuals is a violation of the honor   
	-- code.  I also understand that if I knowingly give my original work to 
	-- another individual is also a violation of the honor code. 
	-------------------------------------------------------------------------

	.text
	main :
		# Put answer, 7, into register $s0
		addi $s0, $0, 0x37 # 7 in hex is 0x37
		
		# Put numbers in registers to be used later
		addi $s1, $0, 1
		lui $s3, 0xFFFF
		
		# Put the ascii values of letters in registers to be used later
		addi $s4, $0, 0x57 # Store W character
		addi $s5, $0, 0x48 # Store H character
		addi $s6, $9, 0x4C # Store L character
	
	loop:
		# wait for user input
		lw $t0, 0($s3) # will be one if there is user input, zero otherwise
		beq $t0, $0, loop # Go back to loop if no user input
		lw $t1, 4($s3) # Else load the input to $t1
		
		# High, low, equal calculation 
		beq $s0, $t1, winner
		slt $t2, $t1, $s0   # $t2 = 1 if guess lower than answer
		beq $t2, $s1, toolow # if guess is too low go to toolow
		j toohigh # Guess is not correct and not less than, so it must be greater than
		
	toolow:	
		# Output the text
		sw $s6, 0xC($s3)
		j loop
		
	toohigh:	
		# Output the text
		sw $s5, 0xC($s3)
		j loop
		
	winner:	
		# Output the text
		sw $s4, 0xC($s3)
		j end
	
	end:	
		# Exit the program
		addi $v0, $0, 10
		syscall


### Debugging
I had some difficulty with getting my data to and from the MMIO simulator. But after learning how to read and save to the addresses need with lw and sw I was able to figure it out. 

### Testing methodology or results
##### Test Number 1

![](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/377d5d3dc508808f5ccf72778cf13f074051bbc5/CE5_Hanson/test_screenshot1.JPG?token=b27654713bd6e1c7d124e83e45dd82f3192e938e)

##### Test Number 2

![](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/377d5d3dc508808f5ccf72778cf13f074051bbc5/CE5_Hanson/test_screenshot2.JPG?token=fc776b748a02d2ca7233c070d128598480a53859)

In both tests seven was the number to guess. Anytime the users guess was too small 'L' would print and anytime the user guessed too high the letter 'H' would print until the correct letter was guessed printing a 'W'.

### Answers to Lab Questions
1) The following addressing modes were used in my program.

PC-Relative Addressing - This type is used for beq instructions like on line 37

Pseudo-direct Addressing - This type is used for jump instructions like on line 44

Base Addressing - This is used in the lw and sw instructions like on 

2) The pseudo instruction mov would have been useful to copy the contents of one register to another. 

3) Maybe a subroutine to wait for user input and compare the input to the answer would have been helpful. But, there weren't really any complicated functions going on here. 

### Observations and Conclusions
The hardest part of this kind of programming for me is keeping track of what values you put where. After I got a good flowchart and pseudo code written it went pretty smoothly. 

### Documentation
Summary on addressing modes came from https://www.cs.umd.edu/class/sum2003/cmsc311/Notes/Mips/addr.html. 