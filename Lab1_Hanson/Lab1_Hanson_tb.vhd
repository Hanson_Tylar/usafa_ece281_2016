-- Vhdl test bench created from schematic C:\Users\C18Tylar.Hanson\Documents\Spring_2016\ECE_281\Xilinx_Projects\Lab1_Hanson\sseg_decoder.sch - Thu Jan 28 13:55:03 2016
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--

--------------------------------------------------------------------
-- Name: Tylar Hanson
-- Date: 28 Jan 2016
-- Course: ECE 281
-- File: Lab1_Hanson_tb.vhd
--
-- Purp: In this lab, I will write, test, and implement a seven-segment
--	display decoder on the Nexys2 Development Board.  I will input a 
-- four-bit value using switches, select one or more seven-segment display
--	using buttons, and the seven-segment display will output the correct 
-- hex digit.
--
-- Doc:	None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY sseg_decoder_sseg_decoder_sch_tb IS
END sseg_decoder_sseg_decoder_sch_tb;
ARCHITECTURE behavioral OF sseg_decoder_sseg_decoder_sch_tb IS 

   COMPONENT sseg_decoder
   PORT( D1	:	IN	STD_LOGIC; 
          D0	:	IN	STD_LOGIC; 
          D3	:	IN	STD_LOGIC; 
          D2	:	IN	STD_LOGIC; 
          a	:	OUT	STD_LOGIC; 
          b	:	OUT	STD_LOGIC; 
          c	:	OUT	STD_LOGIC; 
          d	:	OUT	STD_LOGIC; 
          e	:	OUT	STD_LOGIC; 
          f	:	OUT	STD_LOGIC; 
          g	:	OUT	STD_LOGIC);
   END COMPONENT;

   SIGNAL D1	:	STD_LOGIC;
   SIGNAL D0	:	STD_LOGIC;
   SIGNAL D3	:	STD_LOGIC;
   SIGNAL D2	:	STD_LOGIC;
   SIGNAL a	:	STD_LOGIC;
   SIGNAL b	:	STD_LOGIC;
   SIGNAL c	:	STD_LOGIC;
   SIGNAL d	:	STD_LOGIC;
   SIGNAL e	:	STD_LOGIC;
   SIGNAL f	:	STD_LOGIC;
   SIGNAL g	:	STD_LOGIC;

BEGIN

   UUT: sseg_decoder PORT MAP(
		D3 => D3,
		D2 => D2, 
		D1 => D1,  
		D0 => D0, 
		g => g, 
		f => f, 
		e => e, 
		d => d, 
		c => c, 
		b => b, 
		a => a
   );

-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
	
			D3 <= '0'; D2 <= '0'; D1 <= '0'; D0 <= '0';
			wait for 10 ns;
			
			D3 <= '0'; D2 <= '0'; D1 <= '0'; D0 <= '1';
			wait for 10 ns;
			
			D3 <= '0'; D2 <= '0'; D1 <= '1'; D0 <= '0';
			wait for 10 ns;
			
			D3 <= '0'; D2 <= '0'; D1 <= '1'; D0 <= '1';
			wait for 10 ns;
			
			D3 <= '0'; D2 <= '1'; D1 <= '0'; D0 <= '0';
			wait for 10 ns;
			
			D3 <= '0'; D2 <= '1'; D1 <= '0'; D0 <= '1';
			wait for 10 ns;
			
			D3 <= '0'; D2 <= '1'; D1 <= '1'; D0 <= '0';
			wait for 10 ns;
			
			D3 <= '0'; D2 <= '1'; D1 <= '1'; D0 <= '1';
			wait for 10 ns;
			
			D3 <= '1'; D2 <= '0'; D1 <= '0'; D0 <= '0';
			wait for 10 ns;
			
			D3 <= '1'; D2 <= '0'; D1 <= '0'; D0 <= '1';
			wait for 10 ns;
			
			D3 <= '1'; D2 <= '0'; D1 <= '1'; D0 <= '0';
			wait for 10 ns;
			
			D3 <= '1'; D2 <= '0'; D1 <= '1'; D0 <= '1';
			wait for 10 ns;
			
			D3 <= '1'; D2 <= '1'; D1 <= '0'; D0 <= '0';
			wait for 10 ns;
			
			D3 <= '1'; D2 <= '1'; D1 <= '0'; D0 <= '1';
			wait for 10 ns;
			
			D3 <= '1'; D2 <= '1'; D1 <= '1'; D0 <= '0';
			wait for 10 ns;
			
			D3 <= '1'; D2 <= '1'; D1 <= '1'; D0 <= '1';
			wait for 10 ns;
			
      WAIT; -- will wait forever
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
