# Lab #1 - Seven-Segment Display Decoder

## By C3C Hanson

## Table of Contents
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Preliminary Design](#preliminary-design)
3. [Hardware schematic](#hardware-schematic)
4. [Well-formatted code](#well-formatted-code)
5. [Debugging](#debugging)
6. [Testing methodology or results](#testing-methodology-or-results)
7. [Answers to Lab Questions](#answers-to-lab-questions)
8. [Observations and Conclusions](#observations-and-conclusions)
9. [Documentation](#documentation)
 
### Objectives or Purpose 
In this lab, I will write, test, and implement a seven-segment display decoder on the Nexys2 Development Board.  I will input a four-bit value using switches, select one or more seven-segment display using buttons, and the seven-segment display will output the correct hex digit.

### Preliminary design
![prelab truth table](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/34e476094a07f9e75aedb297717d6a836a56bed1/Lab1_Hanson/prelab_truth_table.JPG?token=09dc14d271ab2d93b809441de09beeefdf00fa93)

### Hardware schematic
`Figure 1. This is the top schematic of my seven-segment display decoder`

![Lab1_Hanson_top_Capture.jpg](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/7de04c773041fa03c56e4712232b7ee452ba98d2/Lab1_Hanson/Lab1_Hanson_top_Capture.JPG?token=02cfc4b3eef2b0ec8ae5d3512d07bd147bf474cf)

`Figure 2. This is the schematic relating the buttons and the displays`

![display_en_Capture.jpg](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/7de04c773041fa03c56e4712232b7ee452ba98d2/Lab1_Hanson/display_en_Capture.JPG?token=26c767dc60738484f5d89debaecba82797e18101)

`Figure 3. This is the schematic relating the switches to individual segments inside the display.`

![sseg_decoder_Capture.jpg](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/7de04c773041fa03c56e4712232b7ee452ba98d2/Lab1_Hanson/sseg_decoder_Capture.JPG?token=f8f6f5968886cfd197c2f4650c8435f1e71271d0)

### Well-formatted code
  `The following code tests the seven segment display against all combinations of a four bit binary input.` 

	--------------------------------------------------------------------
	-- Name: Tylar Hanson
	-- Date: 28 Jan 2016
	-- Course: ECE 281
	-- File: Lab1_Hanson_tb.vhd
	--
	-- Purp: In this lab, I will write, test, and implement a seven-segment
	--	display decoder on the Nexys2 Development Board.  I will input a 
	-- four-bit value using switches, select one or more seven-segment display
	--	using buttons, and the seven-segment display will output the correct 
	-- hex digit.
	--
	-- Doc:	None
	--
	-- Academic Integrity Statement: I certify that, while others may have 
	-- assisted me in brain storming, debugging and validating this program, 
	-- the program itself is my own work. I understand that submitting code 
	-- which is the work of other individuals is a violation of the honor   
	-- code.  I also understand that if I knowingly give my original work to 
	-- another individual is also a violation of the honor code. 
	-------------------------------------------------------------------------
	LIBRARY ieee;
	USE ieee.std_logic_1164.ALL;
	USE ieee.numeric_std.ALL;
	LIBRARY UNISIM;
	USE UNISIM.Vcomponents.ALL;
	ENTITY sseg_decoder_sseg_decoder_sch_tb IS
	END sseg_decoder_sseg_decoder_sch_tb;
	ARCHITECTURE behavioral OF sseg_decoder_sseg_decoder_sch_tb IS 

   	COMPONENT sseg_decoder
   	PORT( D1	:	IN	STD_LOGIC; 
          	D0	:	IN	STD_LOGIC; 
          	D3	:	IN	STD_LOGIC; 
          	D2	:	IN	STD_LOGIC; 
          	a	:	OUT	STD_LOGIC; 
          	b	:	OUT	STD_LOGIC; 
          	c	:	OUT	STD_LOGIC; 
          	d	:	OUT	STD_LOGIC; 
          	e	:	OUT	STD_LOGIC; 
          	f	:	OUT	STD_LOGIC; 
          	g	:	OUT	STD_LOGIC);
   	END COMPONENT;

   	SIGNAL D1	:	STD_LOGIC;
   	SIGNAL D0	:	STD_LOGIC;
   	SIGNAL D3	:	STD_LOGIC;
   	SIGNAL D2	:	STD_LOGIC;
   	SIGNAL a	:	STD_LOGIC;
   	SIGNAL b	:	STD_LOGIC;
   	SIGNAL c	:	STD_LOGIC;
   	SIGNAL d	:	STD_LOGIC;
   	SIGNAL e	:	STD_LOGIC;
   	SIGNAL f	:	STD_LOGIC;
   	SIGNAL g	:	STD_LOGIC;

	BEGIN

   	UUT: sseg_decoder PORT MAP(
			D3 => D3,
			D2 => D2, 
			D1 => D1,  
			D0 => D0, 
			g => g, 
			f => f, 
			e => e, 
			d => d, 
			c => c, 
			b => b, 
			a => a
   	);

	-- *** Test Bench - User Defined Section ***
   	tb : PROCESS
   	BEGIN
	
			D3 <= '0'; D2 <= '0'; D1 <= '0'; D0 <= '0';
			wait for 10 ns;
			
			D3 <= '0'; D2 <= '0'; D1 <= '0'; D0 <= '1';
			wait for 10 ns;
			
			D3 <= '0'; D2 <= '0'; D1 <= '1'; D0 <= '0';
			wait for 10 ns;
			
			D3 <= '0'; D2 <= '0'; D1 <= '1'; D0 <= '1';
			wait for 10 ns;
			
			D3 <= '0'; D2 <= '1'; D1 <= '0'; D0 <= '0';
			wait for 10 ns;
			
			D3 <= '0'; D2 <= '1'; D1 <= '0'; D0 <= '1';
			wait for 10 ns;
			
			D3 <= '0'; D2 <= '1'; D1 <= '1'; D0 <= '0';
			wait for 10 ns;
			
			D3 <= '0'; D2 <= '1'; D1 <= '1'; D0 <= '1';
			wait for 10 ns;
			
			D3 <= '1'; D2 <= '0'; D1 <= '0'; D0 <= '0';
			wait for 10 ns;
			
			D3 <= '1'; D2 <= '0'; D1 <= '0'; D0 <= '1';
			wait for 10 ns;
			
			D3 <= '1'; D2 <= '0'; D1 <= '1'; D0 <= '0';
			wait for 10 ns;
			
			D3 <= '1'; D2 <= '0'; D1 <= '1'; D0 <= '1';
			wait for 10 ns;
			
			D3 <= '1'; D2 <= '1'; D1 <= '0'; D0 <= '0';
			wait for 10 ns;
			
			D3 <= '1'; D2 <= '1'; D1 <= '0'; D0 <= '1';
			wait for 10 ns;
			
			D3 <= '1'; D2 <= '1'; D1 <= '1'; D0 <= '0';
			wait for 10 ns;
			
			D3 <= '1'; D2 <= '1'; D1 <= '1'; D0 <= '1';
			wait for 10 ns;
			
      WAIT; -- will wait forever
   	END PROCESS;
	-- *** End Test Bench - User Defined Section ***

	END;


  `This next section of code tests enabling the seven segment display when its corresponding button is pressed. `

	LIBRARY ieee;
	USE ieee.std_logic_1164.ALL;
	USE ieee.numeric_std.ALL;
	LIBRARY UNISIM;
	USE UNISIM.Vcomponents.ALL;
	ENTITY display_en_display_en_sch_tb IS
	END display_en_display_en_sch_tb;
	ARCHITECTURE behavioral OF display_en_display_en_sch_tb IS 
	
	   COMPONENT display_en
	   PORT( sseg_sel3	:	IN	STD_LOGIC; 
	          sseg_sel2	:	IN	STD_LOGIC; 
	          sseg_sel1	:	IN	STD_LOGIC; 
	          sseg_sel0	:	IN	STD_LOGIC; 
	          sseg_sel_n3	:	OUT	STD_LOGIC; 
	          sseg_sel_n2	:	OUT	STD_LOGIC; 
	          sseg_sel_n1	:	OUT	STD_LOGIC; 
	          sseg_sel_n0	:	OUT	STD_LOGIC);
	   END COMPONENT;
	
	   SIGNAL sseg_sel3	:	STD_LOGIC;
	   SIGNAL sseg_sel2	:	STD_LOGIC;
	   SIGNAL sseg_sel1	:	STD_LOGIC;
	   SIGNAL sseg_sel0	:	STD_LOGIC;
	   SIGNAL sseg_sel_n3	:	STD_LOGIC;
	   SIGNAL sseg_sel_n2	:	STD_LOGIC;
	   SIGNAL sseg_sel_n1	:	STD_LOGIC;
	   SIGNAL sseg_sel_n0	:	STD_LOGIC;
	
	BEGIN
	
	   UUT: display_en PORT MAP(
			sseg_sel3 => sseg_sel3, 
			sseg_sel2 => sseg_sel2, 
			sseg_sel1 => sseg_sel1, 
			sseg_sel0 => sseg_sel0, 
			sseg_sel_n3 => sseg_sel_n3, 
			sseg_sel_n2 => sseg_sel_n2, 
			sseg_sel_n1 => sseg_sel_n1, 
			sseg_sel_n0 => sseg_sel_n0
	   );

	-- *** Test Bench - User Defined Section ***
   	tb : PROCESS
   	BEGIN
	
			sseg_sel0 <= '0'; sseg_sel1 <= '0'; sseg_sel2 <= '0'; sseg_sel3 <= '0';
			wait for 10 ns;
			
			sseg_sel0 <= '0'; sseg_sel1 <= '0'; sseg_sel2 <= '0'; sseg_sel3 <= '1';
			wait for 10 ns;
			
			sseg_sel0 <= '0'; sseg_sel1 <= '0'; sseg_sel2 <= '1'; sseg_sel3 <= '0';
			wait for 10 ns;
			
			sseg_sel0 <= '0'; sseg_sel1 <= '0'; sseg_sel2 <= '1'; sseg_sel3 <= '1';
			wait for 10 ns;
			
			sseg_sel0 <= '0'; sseg_sel1 <= '1'; sseg_sel2 <= '0'; sseg_sel3 <= '0';
			wait for 10 ns;
			
			sseg_sel0 <= '0'; sseg_sel1 <= '1'; sseg_sel2 <= '0'; sseg_sel3 <= '1';
			wait for 10 ns;
			
			sseg_sel0 <= '0'; sseg_sel1 <= '1'; sseg_sel2 <= '1'; sseg_sel3 <= '0';
			wait for 10 ns;
			
			sseg_sel0 <= '0'; sseg_sel1 <= '1'; sseg_sel2 <= '1'; sseg_sel3 <= '1';
			wait for 10 ns;
			
			sseg_sel0 <= '1'; sseg_sel1 <= '0'; sseg_sel2 <= '0'; sseg_sel3 <= '0';
			wait for 10 ns;
			
			sseg_sel0 <= '1'; sseg_sel1 <= '0'; sseg_sel2 <= '0'; sseg_sel3 <= '1';
			wait for 10 ns;
			
			sseg_sel0 <= '1'; sseg_sel1 <= '0'; sseg_sel2 <= '1'; sseg_sel3 <= '0';
			wait for 10 ns;
			
			sseg_sel0 <= '1'; sseg_sel1 <= '0'; sseg_sel2 <= '1'; sseg_sel3 <= '1';
			wait for 10 ns;
			
			sseg_sel0 <= '1'; sseg_sel1 <= '1'; sseg_sel2 <= '0'; sseg_sel3 <= '0';
			wait for 10 ns;
			
			sseg_sel0 <= '1'; sseg_sel1 <= '1'; sseg_sel2 <= '0'; sseg_sel3 <= '1';
			wait for 10 ns;
			
			sseg_sel0 <= '1'; sseg_sel1 <= '1'; sseg_sel2 <= '1'; sseg_sel3 <= '0';
			wait for 10 ns;
			
			sseg_sel0 <= '1'; sseg_sel1 <= '1'; sseg_sel2 <= '1'; sseg_sel3 <= '1';
			wait for 10 ns;
			
      WAIT; -- will wait forever
   	END PROCESS;
	-- *** End Test Bench - User Defined Section ***

	END;

### Debugging

The first time I ran the simulation after building my test bench several of my outputs were wrong. After moving my Sg output to another LUT the outputs were all correct. This is how I knew I messed up something with the decoder. Eventually I learned that the decoder I am using is one-hot and not one-low as I had assumed. I removed all of the inverters from the outputs and the simulation ran correctly. 

### Testing methodology or results

One of the first tasks was to was to have the display power the correct segments according to the letter or number I wanted it to display. Using the code and schematic shown above I ran a simulation with the following results. The hex outlined in blue represents the input. The hex outlined in red represents the output. Table 1 shows us what the hex output should be for a certain hex input. Comparing Table 1 with my simulation waveform gives me the same results.

![Lab1_ISIM_Capture.jpg](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/01a75c6399d8a40c59325d58f68796f10f5ca73b/Lab1_Hanson/Lab1_ISIM_Capture.JPG?token=4f7ddc7d694a86c4154f2c395609909dfb0d4592)

After this test was successfully completed I needed to power the displays when the button below it was pressed. The push buttons on the board are active-high and the displays are active-low. When a button is pressed it gives a high signal then is inverted to send a low signal and enable the display. Looking at the waveform for every input the output is inverted. 

![display_en_ISIM_Capture.jpg](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/01a75c6399d8a40c59325d58f68796f10f5ca73b/Lab1_Hanson/display_en_ISIM_Capture.JPG?token=c28c11036705fb9908982b2437e56c2e3b44fc72)

### Answers to Lab Questions

1. For each output, if you had to implement the full SOP or POS equations in hardware, which one would you choose and why?
	I would choose to do Sum of Products. I would choose these because there about half as many high outputs as there are low outputs, so there would be many fewer terms. Also, I think SOP equations are easier to simplify.
2. 
  Sg = D3'D2'D1'D0' + D3'D2'D1'D0 + D3'D2D1D0
  Sf = D3'D2'D1'D0 + D3'D2'D1D0' + D3'D2'D1D0 + D3'D2D1D0 + D3D2D1'D0' + D3D2D1'D0
  Se = D3'D2'D1'D0 + D3'D2'D1D0 + D3'D2D1'D0' + D3'D2D1'D0 + D3'D2D1D0 + D3D2'D1'D0 .  Sd = D3'D2'D1'D0 + D3'D2D1'D0' + D3'D2D1D0 + D3D2'D1'D0 + D3D2'D1D0' + D3D2D1D0
  Sc = D3'D2'D1D0' + D3D2D1'D0' + D3D2D1D0' + D3D2D1D0
  Sb = D3'D2D1'D0 + D3'D2D1D0' + D3D2'D1D0 + D3D2D1'D0' + D3D2D1D0' + D3D2D1D0'
  Sa = D3'D2'D1'D0 + D3'D2D1'D0' + D3D2'D1D0 + D3D2D1'D0' + D3D2D1'D0

3. Simplify one equation with a K-map.

	|______| D1D0 _____________|
	| D3D2 | 00 | 01 | 11 | 10 |
	| 00   |  0 |  1 |  1 |  1 |
	| 01   |  0 |  0 |  1 |  0 |
	| 11   |  1 |  1 |  0 |  0 |
	| 10   |  0 |  0 |  0 |  0 |
	
	Sf = D3'D2'D0 + D3'D2'D1 + D3'D1D0 + D3D2D1' 

4. A LUT is a look up table. LUT's are memory arrays used to perform logic. LUT's can be used to solve complex problems because for any input the output is already stored. No computation is required, so they are faster and require less processing. 

5. The decoders used in this lab are 'one-low' decoders. So for a four bit input it drives one output low. By inverting these outputs and OR-ing them all together I get a high signal whenever the particular segment needs to be turned on.

6. The purpose of the Lab1 constraints file is this file tells Xilinx how the input/output ports map to actual FPGA pins.
    

### Observations and Conclusions

The purpose of this lab was to create a seven-segment display decoder on the Nexys2 board and by the end I achieved that. I learned how to use Look Up Tables and how a seven-segment display chooses which segments are to be powered. 

### Documentation
None.