<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="D1" />
        <signal name="D0" />
        <signal name="b" />
        <signal name="c" />
        <signal name="d" />
        <signal name="e" />
        <signal name="f" />
        <signal name="g" />
        <signal name="D2" />
        <signal name="D3" />
        <signal name="XLXN_113" />
        <signal name="XLXN_114" />
        <signal name="XLXN_115" />
        <signal name="XLXN_116" />
        <signal name="XLXN_124" />
        <signal name="XLXN_125" />
        <signal name="XLXN_126" />
        <signal name="XLXN_134" />
        <signal name="XLXN_135" />
        <signal name="XLXN_136" />
        <signal name="XLXN_137" />
        <signal name="XLXN_145" />
        <signal name="XLXN_146" />
        <signal name="XLXN_147" />
        <signal name="XLXN_148" />
        <signal name="XLXN_160" />
        <signal name="XLXN_161" />
        <signal name="XLXN_162" />
        <signal name="XLXN_163" />
        <signal name="XLXN_171" />
        <signal name="XLXN_172" />
        <signal name="XLXN_173" />
        <signal name="XLXN_174" />
        <signal name="XLXN_182" />
        <signal name="XLXN_183" />
        <signal name="XLXN_184" />
        <signal name="XLXN_185" />
        <signal name="XLXN_186" />
        <signal name="XLXN_189" />
        <signal name="XLXN_190" />
        <signal name="XLXN_193" />
        <signal name="XLXN_200" />
        <signal name="XLXN_202" />
        <signal name="XLXN_203" />
        <signal name="XLXN_204" />
        <signal name="XLXN_205" />
        <signal name="XLXN_212" />
        <signal name="XLXN_214" />
        <signal name="XLXN_215" />
        <signal name="XLXN_216" />
        <signal name="XLXN_217" />
        <signal name="XLXN_218" />
        <signal name="XLXN_225" />
        <signal name="XLXN_227" />
        <signal name="XLXN_228" />
        <signal name="XLXN_229" />
        <signal name="XLXN_230" />
        <signal name="XLXN_237" />
        <signal name="XLXN_239" />
        <signal name="XLXN_240" />
        <signal name="XLXN_241" />
        <signal name="XLXN_242" />
        <signal name="XLXN_1" />
        <signal name="XLXN_5" />
        <signal name="XLXN_105" />
        <signal name="a" />
        <signal name="XLXN_4" />
        <signal name="XLXN_2" />
        <signal name="XLXN_249" />
        <signal name="XLXN_3" />
        <signal name="XLXN_251" />
        <signal name="XLXN_252" />
        <signal name="XLXN_253" />
        <signal name="XLXN_254" />
        <signal name="XLXN_255" />
        <signal name="XLXN_256" />
        <signal name="XLXN_257" />
        <signal name="XLXN_258" />
        <port polarity="Input" name="D1" />
        <port polarity="Input" name="D0" />
        <port polarity="Output" name="b" />
        <port polarity="Output" name="c" />
        <port polarity="Output" name="d" />
        <port polarity="Output" name="e" />
        <port polarity="Output" name="f" />
        <port polarity="Output" name="g" />
        <port polarity="Input" name="D2" />
        <port polarity="Input" name="D3" />
        <port polarity="Output" name="a" />
        <blockdef name="lut4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="320" y1="-192" y2="-192" x1="384" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <rect width="256" x="64" y="-384" height="320" />
        </blockdef>
        <blockdef name="d4_16e">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <rect width="256" x="64" y="-1152" height="1088" />
            <line x2="320" y1="-1088" y2="-1088" x1="384" />
            <line x2="320" y1="-1024" y2="-1024" x1="384" />
            <line x2="320" y1="-960" y2="-960" x1="384" />
            <line x2="320" y1="-896" y2="-896" x1="384" />
            <line x2="320" y1="-832" y2="-832" x1="384" />
            <line x2="320" y1="-768" y2="-768" x1="384" />
            <line x2="320" y1="-704" y2="-704" x1="384" />
            <line x2="320" y1="-640" y2="-640" x1="384" />
            <line x2="320" y1="-576" y2="-576" x1="384" />
            <line x2="320" y1="-512" y2="-512" x1="384" />
            <line x2="320" y1="-448" y2="-448" x1="384" />
            <line x2="320" y1="-384" y2="-384" x1="384" />
            <line x2="320" y1="-320" y2="-320" x1="384" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="320" y1="-192" y2="-192" x1="384" />
            <line x2="320" y1="-128" y2="-128" x1="384" />
            <line x2="64" y1="-896" y2="-896" x1="0" />
            <line x2="64" y1="-960" y2="-960" x1="0" />
            <line x2="64" y1="-1024" y2="-1024" x1="0" />
            <line x2="64" y1="-1088" y2="-1088" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
        </blockdef>
        <blockdef name="or5">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="48" y1="-128" y2="-128" x1="0" />
            <line x2="72" y1="-192" y2="-192" x1="0" />
            <line x2="48" y1="-256" y2="-256" x1="0" />
            <line x2="48" y1="-320" y2="-320" x1="0" />
            <line x2="192" y1="-192" y2="-192" x1="256" />
            <arc ex="192" ey="-192" sx="112" sy="-144" r="88" cx="116" cy="-232" />
            <line x2="48" y1="-240" y2="-240" x1="112" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <line x2="48" y1="-64" y2="-144" x1="48" />
            <line x2="48" y1="-320" y2="-240" x1="48" />
            <arc ex="112" ey="-240" sx="192" sy="-192" r="88" cx="116" cy="-152" />
            <arc ex="48" ey="-240" sx="48" sy="-144" r="56" cx="16" cy="-192" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <block symbolname="lut4" name="Sb">
            <attr value="D860" name="INIT">
                <trait editname="all:1 sch:0" />
                <trait edittrait="all:1 sch:0" />
                <trait verilog="all:0 dp:1nosynth wsynop:1 wsynth:1" />
                <trait vhdl="all:0 gm:1nosynth wa:1 wd:1" />
                <trait valuetype="BitVector 16 h" />
            </attr>
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="D1" name="I1" />
            <blockpin signalname="D2" name="I2" />
            <blockpin signalname="D3" name="I3" />
            <blockpin signalname="b" name="O" />
        </block>
        <block symbolname="lut4" name="Sc">
            <attr value="D004" name="INIT">
                <trait editname="all:1 sch:0" />
                <trait edittrait="all:1 sch:0" />
                <trait verilog="all:0 dp:1nosynth wsynop:1 wsynth:1" />
                <trait vhdl="all:0 gm:1nosynth wa:1 wd:1" />
                <trait valuetype="BitVector 16 h" />
            </attr>
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="D1" name="I1" />
            <blockpin signalname="D2" name="I2" />
            <blockpin signalname="D3" name="I3" />
            <blockpin signalname="c" name="O" />
        </block>
        <block symbolname="lut4" name="Sd">
            <attr value="8692" name="INIT">
                <trait editname="all:1 sch:0" />
                <trait edittrait="all:1 sch:0" />
                <trait verilog="all:0 dp:1nosynth wsynop:1 wsynth:1" />
                <trait vhdl="all:0 gm:1nosynth wa:1 wd:1" />
                <trait valuetype="BitVector 16 h" />
            </attr>
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="D1" name="I1" />
            <blockpin signalname="D2" name="I2" />
            <blockpin signalname="D3" name="I3" />
            <blockpin signalname="d" name="O" />
        </block>
        <block symbolname="lut4" name="Se">
            <attr value="02BA" name="INIT">
                <trait editname="all:1 sch:0" />
                <trait edittrait="all:1 sch:0" />
                <trait verilog="all:0 dp:1nosynth wsynop:1 wsynth:1" />
                <trait vhdl="all:0 gm:1nosynth wa:1 wd:1" />
                <trait valuetype="BitVector 16 h" />
            </attr>
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="D1" name="I1" />
            <blockpin signalname="D2" name="I2" />
            <blockpin signalname="D3" name="I3" />
            <blockpin signalname="e" name="O" />
        </block>
        <block symbolname="lut4" name="Sf">
            <attr value="308E" name="INIT">
                <trait editname="all:1 sch:0" />
                <trait edittrait="all:1 sch:0" />
                <trait verilog="all:0 dp:1nosynth wsynop:1 wsynth:1" />
                <trait vhdl="all:0 gm:1nosynth wa:1 wd:1" />
                <trait valuetype="BitVector 16 h" />
            </attr>
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="D1" name="I1" />
            <blockpin signalname="D2" name="I2" />
            <blockpin signalname="D3" name="I3" />
            <blockpin signalname="f" name="O" />
        </block>
        <block symbolname="lut4" name="Sg">
            <attr value="0083" name="INIT">
                <trait editname="all:1 sch:0" />
                <trait edittrait="all:1 sch:0" />
                <trait verilog="all:0 dp:1nosynth wsynop:1 wsynth:1" />
                <trait vhdl="all:0 gm:1nosynth wa:1 wd:1" />
                <trait valuetype="BitVector 16 h" />
            </attr>
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="D1" name="I1" />
            <blockpin signalname="D2" name="I2" />
            <blockpin signalname="D3" name="I3" />
            <blockpin signalname="g" name="O" />
        </block>
        <block symbolname="d4_16e" name="Sa_0">
            <blockpin signalname="D0" name="A0" />
            <blockpin signalname="D1" name="A1" />
            <blockpin signalname="D2" name="A2" />
            <blockpin signalname="D3" name="A3" />
            <blockpin signalname="XLXN_105" name="E" />
            <blockpin name="D0" />
            <blockpin signalname="XLXN_1" name="D1" />
            <blockpin name="D10" />
            <blockpin signalname="XLXN_3" name="D11" />
            <blockpin signalname="XLXN_4" name="D12" />
            <blockpin signalname="XLXN_5" name="D13" />
            <blockpin name="D14" />
            <blockpin name="D15" />
            <blockpin name="D2" />
            <blockpin name="D3" />
            <blockpin signalname="XLXN_2" name="D4" />
            <blockpin name="D5" />
            <blockpin name="D6" />
            <blockpin name="D7" />
            <blockpin name="D8" />
            <blockpin name="D9" />
        </block>
        <block symbolname="vcc" name="XLXI_86">
            <blockpin signalname="XLXN_105" name="P" />
        </block>
        <block symbolname="or5" name="XLXI_32">
            <blockpin signalname="XLXN_5" name="I0" />
            <blockpin signalname="XLXN_4" name="I1" />
            <blockpin signalname="XLXN_3" name="I2" />
            <blockpin signalname="XLXN_2" name="I3" />
            <blockpin signalname="XLXN_1" name="I4" />
            <blockpin signalname="a" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="2000" y="2320" name="Sb" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="INIT" x="144" y="-128" type="instance" />
            <attrtext style="fontsize:28;fontname:Arial" attrname="InstName" x="176" y="-192" type="instance" />
        </instance>
        <instance x="2000" y="1984" name="Sc" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="INIT" x="128" y="-160" type="instance" />
            <attrtext style="fontsize:28;fontname:Arial" attrname="InstName" x="160" y="-224" type="instance" />
        </instance>
        <instance x="2000" y="1648" name="Sd" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="INIT" x="128" y="-144" type="instance" />
            <attrtext style="fontsize:28;fontname:Arial" attrname="InstName" x="160" y="-224" type="instance" />
        </instance>
        <instance x="2000" y="1312" name="Se" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="INIT" x="128" y="-176" type="instance" />
            <attrtext style="fontsize:28;fontname:Arial" attrname="InstName" x="176" y="-240" type="instance" />
        </instance>
        <instance x="2000" y="976" name="Sf" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="INIT" x="112" y="-158" type="instance" />
            <attrtext style="fontsize:28;fontname:Arial" attrname="InstName" x="176" y="-224" type="instance" />
        </instance>
        <branch name="b">
            <wire x2="2416" y1="2128" y2="2128" x1="2384" />
        </branch>
        <iomarker fontsize="28" x="2416" y="2128" name="b" orien="R0" />
        <branch name="c">
            <wire x2="2416" y1="1792" y2="1792" x1="2384" />
        </branch>
        <iomarker fontsize="28" x="2416" y="1792" name="c" orien="R0" />
        <branch name="d">
            <wire x2="2416" y1="1456" y2="1456" x1="2384" />
        </branch>
        <iomarker fontsize="28" x="2416" y="1456" name="d" orien="R0" />
        <branch name="e">
            <wire x2="2416" y1="1120" y2="1120" x1="2384" />
        </branch>
        <iomarker fontsize="28" x="2416" y="1120" name="e" orien="R0" />
        <branch name="f">
            <wire x2="2416" y1="784" y2="784" x1="2384" />
        </branch>
        <iomarker fontsize="28" x="2416" y="784" name="f" orien="R0" />
        <branch name="g">
            <wire x2="2416" y1="448" y2="448" x1="2384" />
        </branch>
        <iomarker fontsize="28" x="2416" y="448" name="g" orien="R0" />
        <instance x="2000" y="640" name="Sg" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial" attrname="InstName" x="160" y="-252" type="instance" />
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="INIT" x="128" y="-172" type="instance" />
        </instance>
        <instance x="960" y="1808" name="Sa_0" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial" attrname="InstName" x="176" y="-624" type="instance" />
        </instance>
        <branch name="XLXN_1">
            <wire x2="1472" y1="784" y2="784" x1="1344" />
            <wire x2="1472" y1="784" y2="1056" x1="1472" />
        </branch>
        <branch name="XLXN_5">
            <wire x2="1472" y1="1552" y2="1552" x1="1344" />
            <wire x2="1472" y1="1312" y2="1552" x1="1472" />
        </branch>
        <instance x="832" y="1664" name="XLXI_86" orien="R0" />
        <branch name="XLXN_105">
            <wire x2="896" y1="1664" y2="1680" x1="896" />
            <wire x2="960" y1="1680" y2="1680" x1="896" />
        </branch>
        <branch name="a">
            <wire x2="1744" y1="1184" y2="1184" x1="1728" />
        </branch>
        <branch name="XLXN_4">
            <wire x2="1408" y1="1488" y2="1488" x1="1344" />
            <wire x2="1408" y1="1248" y2="1488" x1="1408" />
            <wire x2="1472" y1="1248" y2="1248" x1="1408" />
        </branch>
        <instance x="1472" y="1376" name="XLXI_32" orien="R0" />
        <branch name="XLXN_2">
            <wire x2="1360" y1="976" y2="976" x1="1344" />
            <wire x2="1360" y1="976" y2="1120" x1="1360" />
            <wire x2="1472" y1="1120" y2="1120" x1="1360" />
        </branch>
        <branch name="XLXN_3">
            <wire x2="1360" y1="1424" y2="1424" x1="1344" />
            <wire x2="1472" y1="1184" y2="1184" x1="1360" />
            <wire x2="1360" y1="1184" y2="1424" x1="1360" />
        </branch>
        <iomarker fontsize="28" x="1744" y="1184" name="a" orien="R0" />
        <branch name="D0">
            <wire x2="864" y1="512" y2="512" x1="576" />
            <wire x2="864" y1="512" y2="720" x1="864" />
            <wire x2="960" y1="720" y2="720" x1="864" />
            <wire x2="1808" y1="512" y2="512" x1="864" />
            <wire x2="2000" y1="512" y2="512" x1="1808" />
            <wire x2="1808" y1="512" y2="848" x1="1808" />
            <wire x2="2000" y1="848" y2="848" x1="1808" />
            <wire x2="1808" y1="848" y2="1184" x1="1808" />
            <wire x2="2000" y1="1184" y2="1184" x1="1808" />
            <wire x2="1808" y1="1184" y2="1520" x1="1808" />
            <wire x2="2000" y1="1520" y2="1520" x1="1808" />
            <wire x2="1808" y1="1520" y2="1856" x1="1808" />
            <wire x2="2000" y1="1856" y2="1856" x1="1808" />
            <wire x2="1808" y1="1856" y2="2192" x1="1808" />
            <wire x2="2000" y1="2192" y2="2192" x1="1808" />
        </branch>
        <branch name="D1">
            <wire x2="800" y1="448" y2="448" x1="576" />
            <wire x2="1856" y1="448" y2="448" x1="800" />
            <wire x2="2000" y1="448" y2="448" x1="1856" />
            <wire x2="1856" y1="448" y2="784" x1="1856" />
            <wire x2="2000" y1="784" y2="784" x1="1856" />
            <wire x2="1856" y1="784" y2="1120" x1="1856" />
            <wire x2="2000" y1="1120" y2="1120" x1="1856" />
            <wire x2="1856" y1="1120" y2="1456" x1="1856" />
            <wire x2="2000" y1="1456" y2="1456" x1="1856" />
            <wire x2="1856" y1="1456" y2="1792" x1="1856" />
            <wire x2="2000" y1="1792" y2="1792" x1="1856" />
            <wire x2="1856" y1="1792" y2="2128" x1="1856" />
            <wire x2="2000" y1="2128" y2="2128" x1="1856" />
            <wire x2="800" y1="448" y2="784" x1="800" />
            <wire x2="960" y1="784" y2="784" x1="800" />
        </branch>
        <branch name="D2">
            <wire x2="720" y1="384" y2="384" x1="576" />
            <wire x2="1904" y1="384" y2="384" x1="720" />
            <wire x2="2000" y1="384" y2="384" x1="1904" />
            <wire x2="1904" y1="384" y2="720" x1="1904" />
            <wire x2="2000" y1="720" y2="720" x1="1904" />
            <wire x2="1904" y1="720" y2="1056" x1="1904" />
            <wire x2="2000" y1="1056" y2="1056" x1="1904" />
            <wire x2="1904" y1="1056" y2="1392" x1="1904" />
            <wire x2="2000" y1="1392" y2="1392" x1="1904" />
            <wire x2="1904" y1="1392" y2="1728" x1="1904" />
            <wire x2="2000" y1="1728" y2="1728" x1="1904" />
            <wire x2="1904" y1="1728" y2="2064" x1="1904" />
            <wire x2="2000" y1="2064" y2="2064" x1="1904" />
            <wire x2="720" y1="384" y2="848" x1="720" />
            <wire x2="960" y1="848" y2="848" x1="720" />
        </branch>
        <branch name="D3">
            <wire x2="656" y1="320" y2="320" x1="576" />
            <wire x2="1952" y1="320" y2="320" x1="656" />
            <wire x2="2000" y1="320" y2="320" x1="1952" />
            <wire x2="1952" y1="320" y2="656" x1="1952" />
            <wire x2="2000" y1="656" y2="656" x1="1952" />
            <wire x2="1952" y1="656" y2="992" x1="1952" />
            <wire x2="2000" y1="992" y2="992" x1="1952" />
            <wire x2="1952" y1="992" y2="1328" x1="1952" />
            <wire x2="2000" y1="1328" y2="1328" x1="1952" />
            <wire x2="1952" y1="1328" y2="1664" x1="1952" />
            <wire x2="2000" y1="1664" y2="1664" x1="1952" />
            <wire x2="1952" y1="1664" y2="2000" x1="1952" />
            <wire x2="2000" y1="2000" y2="2000" x1="1952" />
            <wire x2="656" y1="320" y2="912" x1="656" />
            <wire x2="960" y1="912" y2="912" x1="656" />
        </branch>
        <iomarker fontsize="28" x="576" y="320" name="D3" orien="R180" />
        <iomarker fontsize="28" x="576" y="384" name="D2" orien="R180" />
        <iomarker fontsize="28" x="576" y="448" name="D1" orien="R180" />
        <iomarker fontsize="28" x="576" y="512" name="D0" orien="R180" />
    </sheet>
</drawing>