----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:47:24 03/15/2016 
-- Design Name: 
-- Module Name:    nibble_to_sseg - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity nibble_to_sseg is
    Port ( nibble : in  STD_LOGIC_VECTOR (3 downto 0);
           sseg : out  STD_LOGIC_VECTOR (7 downto 0));
end nibble_to_sseg;

architecture Behavioral of nibble_to_sseg is

begin

	sseg <= x"81" when nibble = "0000" else
				x"CF" when nibble = "0001" else
				x"92" when nibble = "0010" else
				x"86" when nibble = "0011" else
				x"CC" when nibble = "0100" else
				x"A4" when nibble = "0101" else
				x"A0" when nibble = "0110" else
				x"8F" when nibble = "0111" else
				x"80" when nibble = "1000" else
				x"8C" when nibble = "1001" else
				x"88" when nibble = "1010" else
				x"E0" when nibble = "1011" else
				x"F2" when nibble = "1100" else
				x"C2" when nibble = "1101" else
				x"B0" when nibble = "1110" else
				x"B8" when nibble = "1111" else
				x"FF";
				
end Behavioral;

