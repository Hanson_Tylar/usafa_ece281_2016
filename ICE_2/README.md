# In Class Exercise #2 - Nibble to SSEG

## By C3C Hanson

## Table of Contents
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Preliminary Design](#preliminary-design)
3. [Well-formatted code](#well-formatted-code)
4. [Testing methodology or results](#testing-methodology-or-results)
5. [Documentation](#documentation)
 
### Objectives or Purpose 
Produce the appropriate output on the seven-segment display given an input character to display.

### Preliminary design

###### Table 1: Seven Segment Display Truth table
![](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/487ef4064a66d08d24b7ee3d8abe5549c774b420/ICE_2/ICE_2_truth_table.JPG?token=34d00938781891a4c6b4469e0020df505a4b2282)

### Well-formatted code

###### VHDL code for  nibble to sseg
--------------------------------------------------------------------
	-- Name: Tylar Hanson
	-- Date: 15 March 2016
	-- Course:	ECE_281
	-- File: nibble_to_sseg.vhd
	-- HW:	ICE_2
	--
	-- Purp: Produce the appropriate output on the seven-segment display
	-- given an input character to display.
	--
	-- Doc:	None
	--
	-- Academic Integrity Statement: I certify that, while others may have 
	-- assisted me in brain storming, debugging and validating this program, 
	-- the program itself is my own work. I understand that submitting code 
	-- which is the work of other individuals is a violation of the honor   
	-- code.  I also understand that if I knowingly give my original work to 
	-- another individual is also a violation of the honor code. 
	-------------------------------------------------------------------------
	library IEEE;
	use IEEE.STD_LOGIC_1164.ALL;
	
	-- Uncomment the following library declaration if using
	-- arithmetic functions with Signed or Unsigned values
	--use IEEE.NUMERIC_STD.ALL;
	
	-- Uncomment the following library declaration if instantiating
	-- any Xilinx primitives in this code.
	--library UNISIM;
	--use UNISIM.VComponents.all;
	
	entity nibble_to_sseg is
	    Port ( nibble : in  STD_LOGIC_VECTOR (3 downto 0);
	           sseg : out  STD_LOGIC_VECTOR (7 downto 0));
	end nibble_to_sseg;
	
	architecture Behavioral of nibble_to_sseg is
	
	begin

		sseg <= x"81" when nibble = "0000" else
					x"CF" when nibble = "0001" else
					x"92" when nibble = "0010" else
					x"86" when nibble = "0011" else
					x"CC" when nibble = "0100" else
					x"A4" when nibble = "0101" else
					x"A0" when nibble = "0110" else
					x"8F" when nibble = "0111" else
					x"80" when nibble = "1000" else
					x"8C" when nibble = "1001" else
					x"88" when nibble = "1010" else
					x"E0" when nibble = "1011" else
					x"F2" when nibble = "1100" else
					x"C2" when nibble = "1101" else
					x"B0" when nibble = "1110" else
					x"B8" when nibble = "1111" else
					x"FF";
					
	end Behavioral;

###### VHDL code for nibble to sseg test bench
--------------------------------------------------------------------
	-- Name: Tylar Hanson
	-- Date: 15 March 2016
	-- Course:	ECE_281
	-- File: nibble_to_sseg_tb.vhd
	-- HW:	ICE_2
	--
	-- Purp: Produce the appropriate output on the seven-segment display
	-- given an input character to display.
	--
	-- Doc:	None
	--
	-- Academic Integrity Statement: I certify that, while others may have 
	-- assisted me in brain storming, debugging and validating this program, 
	-- the program itself is my own work. I understand that submitting code 
	-- which is the work of other individuals is a violation of the honor   
	-- code.  I also understand that if I knowingly give my original work to 
	-- another individual is also a violation of the honor code. 
	-------------------------------------------------------------------------
	LIBRARY ieee;
	USE ieee.std_logic_1164.ALL;
	 
	-- Uncomment the following library declaration if using
	-- arithmetic functions with Signed or Unsigned values
	--USE ieee.numeric_std.ALL;
	 
	ENTITY nibble_to_sseg_tb IS
	END nibble_to_sseg_tb;
	 
	ARCHITECTURE behavior OF nibble_to_sseg_tb IS 
	 
	    -- Component Declaration for the Unit Under Test (UUT)
	 
	    COMPONENT nibble_to_sseg
	    PORT(
	         nibble : IN  std_logic_vector(3 downto 0);
	         sseg : OUT  std_logic_vector(7 downto 0)
	        );
	    END COMPONENT;
	    
	
	   --Inputs
	   signal nibble : std_logic_vector(3 downto 0) := (others => '0');
	
	 	--Outputs
	   signal sseg : std_logic_vector(7 downto 0);
	   -- No clocks detected in port list. Replace <clock> below with 
	   -- appropriate port name 
	 
	 
	BEGIN
 
		-- Instantiate the Unit Under Test (UUT)
	   uut: nibble_to_sseg PORT MAP (
	          nibble => nibble,
	          sseg => sseg
	        );
	 
	   -- Stimulus process
	   stim_proc: process
	   begin		
	      -- hold reset state for 50 ns.
	      wait for 50 ns;	
	
	      -- insert stimulus here 
			nibble <= x"0";
			wait for 10 ns;
			ASSERT sseg = x"81" REPORT "Test 001 Failed." SEVERITY FAILURE;
			
			nibble <= x"1";
			wait for 10 ns;
			ASSERT sseg = x"Cf" REPORT "Test 001 Failed." SEVERITY FAILURE;
			
			nibble <= x"2";
			wait for 10 ns;
			ASSERT sseg = x"92" REPORT "Test 001 Failed." SEVERITY FAILURE;
			
			nibble <= x"3";
			wait for 10 ns;
			ASSERT sseg = x"86" REPORT "Test 001 Failed." SEVERITY FAILURE;
		
			nibble <= x"4";
			wait for 10 ns;
			ASSERT sseg = x"CC" REPORT "Test 001 Failed." SEVERITY FAILURE;
			
			nibble <= x"5";
			wait for 10 ns;
			ASSERT sseg = x"A4" REPORT "Test 001 Failed." SEVERITY FAILURE;
			
			nibble <= x"6";
			wait for 10 ns;
			ASSERT sseg = x"A0" REPORT "Test 001 Failed." SEVERITY FAILURE;
			
			nibble <= x"7";
			wait for 10 ns;
			ASSERT sseg = x"8F" REPORT "Test 001 Failed." SEVERITY FAILURE;
			
			nibble <= x"8";
			wait for 10 ns;
			ASSERT sseg = x"80" REPORT "Test 001 Failed." SEVERITY FAILURE;
			
			nibble <= x"9";
			wait for 10 ns;
			ASSERT sseg = x"8C" REPORT "Test 001 Failed." SEVERITY FAILURE;
			
			nibble <= x"A";
			wait for 10 ns;
			ASSERT sseg = x"88" REPORT "Test 001 Failed." SEVERITY FAILURE;
			
			nibble <= x"B";
			wait for 10 ns;
			ASSERT sseg = x"E0" REPORT "Test 001 Failed." SEVERITY FAILURE;
		
			nibble <= x"C";
			wait for 10 ns;
			ASSERT sseg = x"F2" REPORT "Test 001 Failed." SEVERITY FAILURE;
			
			nibble <= x"D";
			wait for 10 ns;
			ASSERT sseg = x"C2" REPORT "Test 001 Failed." SEVERITY FAILURE;
			
			nibble <= x"E";
			wait for 10 ns;
			ASSERT sseg = x"B0" REPORT "Test 001 Failed." SEVERITY FAILURE;
			
			nibble <= x"F";
			wait for 10 ns;
			ASSERT sseg = x"B8" REPORT "Test 001 Failed." SEVERITY FAILURE;
			
			ASSERT TRUE = FALSE; REPORT "Correct circuit behavior for all segments." SEVERITY FAILURE;
	      wait;
	   end process;
	
	END;

### Testing methodology or results

###### Simulation results waveform
![](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/b991b30cb3ac135c1d9bab8bf1dbd6b9dca4b133/ICE_2/nibble_to_sseg_tb_waveform.JPG?token=83db37996b5a4193c980219857569237f700cb1a)

###### Simulation console window
![](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/b991b30cb3ac135c1d9bab8bf1dbd6b9dca4b133/ICE_2/nibble_to_sseg_tb_console.JPG?token=406012092cdec3a39fe2827a326dad29b2fcd401)

### Documentation
None.