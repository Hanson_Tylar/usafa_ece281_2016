--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   12:27:19 03/15/2016
-- Design Name:   
-- Module Name:   C:/Users/C18Tylar.Hanson/Documents/Spring_2016/ECE_281/Xilinx_Projects/Lesson22_CE/nibble_to_sseg_tb.vhd
-- Project Name:  Lesson22_CE
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: nibble_to_sseg
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY nibble_to_sseg_tb IS
END nibble_to_sseg_tb;
 
ARCHITECTURE behavior OF nibble_to_sseg_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT nibble_to_sseg
    PORT(
         nibble : IN  std_logic_vector(3 downto 0);
         sseg : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal nibble : std_logic_vector(3 downto 0) := (others => '0');

 	--Outputs
   signal sseg : std_logic_vector(7 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: nibble_to_sseg PORT MAP (
          nibble => nibble,
          sseg => sseg
        );
 
   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 50 ns.
      wait for 50 ns;	

      -- insert stimulus here 
		nibble <= x"0";
		wait for 10 ns;
		ASSERT sseg = x"81" REPORT "Test 001 Failed." SEVERITY FAILURE;
		
		nibble <= x"1";
		wait for 10 ns;
		ASSERT sseg = x"Cf" REPORT "Test 001 Failed." SEVERITY FAILURE;
		
		nibble <= x"2";
		wait for 10 ns;
		ASSERT sseg = x"92" REPORT "Test 001 Failed." SEVERITY FAILURE;
		
		nibble <= x"3";
		wait for 10 ns;
		ASSERT sseg = x"86" REPORT "Test 001 Failed." SEVERITY FAILURE;
		
		nibble <= x"4";
		wait for 10 ns;
		ASSERT sseg = x"CC" REPORT "Test 001 Failed." SEVERITY FAILURE;
		
		nibble <= x"5";
		wait for 10 ns;
		ASSERT sseg = x"A4" REPORT "Test 001 Failed." SEVERITY FAILURE;
		
		nibble <= x"6";
		wait for 10 ns;
		ASSERT sseg = x"A0" REPORT "Test 001 Failed." SEVERITY FAILURE;
		
		nibble <= x"7";
		wait for 10 ns;
		ASSERT sseg = x"8F" REPORT "Test 001 Failed." SEVERITY FAILURE;
		
		nibble <= x"8";
		wait for 10 ns;
		ASSERT sseg = x"80" REPORT "Test 001 Failed." SEVERITY FAILURE;
		
		nibble <= x"9";
		wait for 10 ns;
		ASSERT sseg = x"8C" REPORT "Test 001 Failed." SEVERITY FAILURE;
		
		nibble <= x"A";
		wait for 10 ns;
		ASSERT sseg = x"88" REPORT "Test 001 Failed." SEVERITY FAILURE;
		
		nibble <= x"B";
		wait for 10 ns;
		ASSERT sseg = x"E0" REPORT "Test 001 Failed." SEVERITY FAILURE;
		
		nibble <= x"C";
		wait for 10 ns;
		ASSERT sseg = x"F2" REPORT "Test 001 Failed." SEVERITY FAILURE;
		
		nibble <= x"D";
		wait for 10 ns;
		ASSERT sseg = x"C2" REPORT "Test 001 Failed." SEVERITY FAILURE;
		
		nibble <= x"E";
		wait for 10 ns;
		ASSERT sseg = x"B0" REPORT "Test 001 Failed." SEVERITY FAILURE;
		
		nibble <= x"F";
		wait for 10 ns;
		ASSERT sseg = x"B8" REPORT "Test 001 Failed." SEVERITY FAILURE;
		
		ASSERT TRUE = FALSE; REPORT "Correct circuit behavior for all segments." SEVERITY FAILURE;
      wait;
   end process;

END;
