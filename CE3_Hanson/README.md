# Computer Exercise #3 - Elevator Controller

## By C3C Tylar Hanson

## Table of Contents
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Preliminary Design](#preliminary-design)
3. [Well-formatted code](#well-formatted-code)
4. [Debugging](#debugging)
5. [Testing methodology or results](#testing-methodology-or-results)
6. [Answers to Lab Questions](#answers-to-lab-questions)
7. [Observations and Conclusions](#observations-and-conclusions)
8. [Documentation](#documentation)
 
### Objectives or Purpose 
The purpose of this exercise is to design an elevator controller that will traverse four floors (numbered 1 to 4). In this lab, I will create and simulate a synchronous sequential logic system, I will practice using VHDL to build a provided state diagram, and I will also learn how to write a testbench for a synchronous system to fully simulate the state machine’s operation.

### Preliminary design
The first step in creating a finite state machine is to create a state diagram. For this exercise the below diagram was provided. The input signals are Up_Down (1 to go up, 0 to go down) and Stop( 1 to stop, 0 to not stop). The output corresponds to the floor number in binary.

![](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/3bad19911f72911f485b65182c0971b76d4e81f0/CE3_Hanson/state_diagram.JPG?token=262f7f15ac68a8306d2adbc4f06f8cd65cbc4687)

### Well-formatted code

The code below is a Moore Machine implementation of this design.

	--------------------------------------------------------------------
	-- Name: Tylar Hanson
	-- Date: 8 MAR 2016
	-- Course: ECE 281
	-- File: MooreElevatorController_shell.vhd
	-- HW: CE #3 - Elevator Controller
	--
	-- Purp: Design an elevator controller that will traverse four floors
	-- (numbered 1 to 4). 
	--
	-- Doc: None.
	--
	-- Academic Integrity Statement: I certify that, while others may have 
	-- assisted me in brain storming, debugging and validating this program, 
	-- the program itself is my own work. I understand that submitting code 
	-- which is the work of other individuals is a violation of the honor   
	-- code.  I also understand that if I knowingly give my original work to 
	-- another individual is also a violation of the honor code. 
	------------------------------------------------------------------------- 
	library IEEE;
	use IEEE.STD_LOGIC_1164.ALL;
	
	-- Uncomment the following library declaration if using
	-- arithmetic functions with Signed or Unsigned values
	--use IEEE.NUMERIC_STD.ALL;
	
	-- Uncomment the following library declaration if instantiating
	-- any Xilinx primitives in this code.
	--library UNISIM;
	--use UNISIM.VComponents.all;
	
	entity MooreElevatorController_Shell is
	    Port ( clk : in  STD_LOGIC;
	           reset : in  STD_LOGIC;
	           stop : in  STD_LOGIC;
	           up_down : in  STD_LOGIC;
	           floor : out  STD_LOGIC_VECTOR (3 downto 0));
	end MooreElevatorController_Shell;
	
	architecture Behavioral of MooreElevatorController_Shell is
	
	--Below you create a new variable type! You also define what values that 
	--variable type can take on. Now you can assign a signal as 
	--"floor_state_type" the same way you'd assign a signal as std_logic 
	type floor_state_type is (floor1, floor2, floor3, floor4);
	
	--Here you create a variable "floor_state" that can take on the values
	--defined above. Neat-o!
	signal floor_state : floor_state_type;

	begin
	---------------------------------------------
	--Below you will code your next-state process
	---------------------------------------------
	
	--This line will set up a process that is sensitive to the clock
	floor_state_machine: process(clk)
	begin
		--clk'event and clk='1' is VHDL-speak for a rising edge
		-- if clk'event and clk='1' then
		if rising_edge(clk) then
			--reset is active high and will return the elevator to floor1
			--Question: is reset synchronous or asynchronous?
			if reset='1' then
				floor_state <= floor1;
			--now we will code our next-state logic
			else
				case floor_state is
					--when our current state is floor1
					when floor1 =>
						--if up_down is set to "go up" and stop is set to 
						--"don't stop" which floor do we want to go to?
						if (up_down='1' and stop='0') then 
							--floor2 right?? This makes sense!
							floor_state <= floor2;
						--otherwise we're going to stay at floor1
						else
							floor_state <= floor1;
						end if;
					--when our current state is floor2
	--COMPLETE THE NEXT STATE LOGIC FOR FLOOR 2.  THE COMMENTS SHOULD HELP.		
					when floor2 => 
						--if up_down is set to "go up" and stop is set to
						--"don't stop" which floor do we want to go to?
						if (up_down='1' and stop='0') then 
							floor_state <= floor3;
						--if up_down is set to "go down" and stop is set to 
						--"don't stop" which floor do we want to go to?
						elsif (up_down='0' and stop='0') then 
							floor_state <= floor1;
						--otherwise we're going to stay at floor2
						else
							floor_state <= floor2;
						end if;
					
	--COMPLETE THE NEXT STATE LOGIC ASSIGNMENTS FOR FLOORS 3 AND 4
					when floor3 =>
						if (up_down='1' and stop='0') then 
							floor_state <= floor4;
						elsif (up_down='0' and stop='0') then 
							floor_state <= floor2;
						else
							floor_state <= floor3;
						end if;
					when floor4 =>
						if (up_down='0' and stop='0') then 
							floor_state <= floor3;
						else 
							floor_state <= floor4;
						end if;
				
					--This line accounts for phantom states
					when others =>
						floor_state <= floor1;
				end case;
			end if;
		end if;
	end process;
	
	-- Here you define your output logic. Finish the statements below
	floor <= "0001" when (floor_state = floor1) else
				"0010" when (floor_state = floor2) else
				"0011" when (floor_state = floor3) else
				"0100" when (floor_state = floor4) else
				"0001";
	
	end Behavioral;

After making a Moore Machine I made the same design as a Mealy Machine. 

	--------------------------------------------------------------------
	-- Name: Tylar Hanson
	-- Date: 8 MAR 2016
	-- Course: ECE 281
	-- File: MealyElevatorController_Shell.vhd
	-- HW: CE #3 - Elevator Controller
	--
	-- Purp: Design an elevator controller that will traverse four floors
	-- (numbered 1 to 4). 
	--
	-- Doc: None
	--
	-- Academic Integrity Statement: I certify that, while others may have 
	-- assisted me in brain storming, debugging and validating this program, 
	-- the program itself is my own work. I understand that submitting code 
	-- which is the work of other individuals is a violation of the honor   
	-- code.  I also understand that if I knowingly give my original work to 
	-- another individual is also a violation of the honor code. 
	-------------------------------------------------------------------------
	library IEEE;
	use IEEE.STD_LOGIC_1164.ALL;
	
	-- Uncomment the following library declaration if using
	-- arithmetic functions with Signed or Unsigned values
	--use IEEE.NUMERIC_STD.ALL;
	
	-- Uncomment the following library declaration if instantiating
	-- any Xilinx primitives in this code.
	--library UNISIM;
	--use UNISIM.VComponents.all;
	
	entity MealyElevatorController_Shell is
	    Port ( clk : in  STD_LOGIC;
	           reset : in  STD_LOGIC;
	           stop : in  STD_LOGIC;
	           up_down : in  STD_LOGIC;
	           floor : out  STD_LOGIC_VECTOR (3 downto 0);
				  nextfloor : out std_logic_vector (3 downto 0));
	end MealyElevatorController_Shell;
	
	architecture Behavioral of MealyElevatorController_Shell is
	
	type floor_state_type is (floor1, floor2, floor3, floor4);
	
	signal floor_state : floor_state_type;
	
	begin

	---------------------------------------------------------
	--Code your Mealy machine next-state process below
	--Question: Will it be different from your Moore Machine?
	---------------------------------------------------------
	floor_state_machine: process(clk)
	begin
		if rising_edge(clk) then
			--reset is active high and will return the elevator to floor1
			--Question: is reset synchronous or asynchronous?
			if reset='1' then
				floor_state <= floor1;
			--now we will code our next-state logic
			else
				case floor_state is
					--when our current state is floor1
					when floor1 =>
						--if up_down is set to "go up" and stop is set to 
						--"don't stop" which floor do we want to go to?
						if (up_down='1' and stop='0') then 
							--floor2 right?? This makes sense!
							floor_state <= floor2;
						--otherwise we're going to stay at floor1
						else
							floor_state <= floor1;
						end if;
					--when our current state is floor2
					when floor2 => 
						--if up_down is set to "go up" and stop is set to
						--"don't stop" which floor do we want to go to?
						if (up_down='1' and stop='0') then 
							floor_state <= floor3;
						--if up_down is set to "go down" and stop is set to 
						--"don't stop" which floor do we want to go to?
						elsif (up_down='0' and stop='0') then 
							floor_state <= floor1;
						--otherwise we're going to stay at floor2
						else
							floor_state <= floor2;
						end if;
					when floor3 =>
						if (up_down='1' and stop='0') then 
							floor_state <= floor4;
						elsif (up_down='0' and stop='0') then 
							floor_state <= floor2;
						else
							floor_state <= floor3;
						end if;
					when floor4 =>
						if (up_down='0' and stop='0') then 
							floor_state <= floor3;
						else 
							floor_state <= floor4;
						end if;
					
					--This line accounts for phantom states
					when others =>
						floor_state <= floor1;
				end case;
			end if;
		end if;
	end process;
	
	-----------------------------------------------------------
	--Code your Ouput Logic for your Mealy machine below
	--Remember, now you have 2 outputs (floor and nextfloor)
	-----------------------------------------------------------
	floor <= "0001" when (floor_state = floor1) else
				"0010" when (floor_state = floor2) else
				"0011" when (floor_state = floor3) else
				"0100" when (floor_state = floor4) else
				"0001";
				
	nextfloor <= "0001" when ((reset = '1') or (floor_state = floor1 and stop = '1') or (floor_state = floor1 and up_down = '0' and stop = '0') or (floor_state = floor2 and up_down = '0' and stop = '0')) else
					 "0010" when ((floor_state = floor2 and stop = '1') or (floor_state = floor1 and up_down = '1' and stop = '0') or (floor_state = floor3 and up_down = '0' and stop = '0')) else
					 "0011" when ((floor_state = floor3 and stop = '1') or (floor_state = floor2 and up_down = '1' and stop = '0') or (floor_state = floor4 and up_down = '0' and stop = '0')) else
					 "0100" when ((floor_state = floor4 and stop = '1') or (floor_state = floor3 and up_down = '1' and stop = '0') or (floor_state = floor4 and up_down = '1' and stop = '0')) else
					 "0001";
	end Behavioral;

### Debugging
You should be keeping track of issues as you go along.  I didn't have any problems is not a good answer.  Describe the problems you had and what you did to fix it.  Again this is where I would say commit early and often and start your notebook when you start your code.

### Testing methodology or results

##### Software Testing
The first test was to start the elevator at floor one and go up to floor four, stopping at each floor for at least two clock cycles. Once stopped on floor four, the elevator should return to floor one without stopping. This simulation shows that my design is functional. It starts on the first floor, shown by the green signal on bottom, and makes stops on floors two, three, and four for six clock cycles each. It then goes to floors three, two, and one without stopping on each floor. 

![](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/455f2c7bffb16f8902852e6722e67cc6201f55d5/CE3_Hanson/CE3_ISIM.JPG?token=7c329ed7195db132e0ae6dfa1491f01abf6a9489)

The next test was the MealyElevatorController. The simulation is the same as above with an additional, nextfloor, output. The floor output is as I expected, and the same as the test before, but the nextfloor isn't quite what I though it was going to be. The stop signal takes half a clock cycle to be applied so the nextfloor output almost looks like a glitch until the stop is applied. It is right though that that when on floor2 that the next floor is floor3 until the stop signal is applied. 

![](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/ceb35fe8cb6f65d434b3cdb4da399eb8e8be615e/CE3_Hanson/CE3_Mealy_ISIM.JPG?token=0f9928f181a1edc578204c3bce4a77671a1e8d80)

A day after writing these test benches I added ASSERT and REPORT lines to each test case after the wait period. All of the tests passed successfully. This helped me verify that what I thought the design was doing was actually what happened in the simulation. 

### Answers to Lab Questions
In MooreElevatorController Shell I changed line 61 from "if clk'event and clk='1' then" to "if rising_edge(clk) then". Doing this reduced the number of logic gates needed to evaluate the statement. The reset in this machine is synchronous.

The clock frequency for a period of 10ns is 100 MHz.

The period of a 50 MHz clock is 20ns.

### Observations and Conclusions
I observed that the only difference between the Moore and Mealy machine was that the Mealy had an extra output signal. This lab leaves me wondering why we made two different machines for this design. Is Mealy better or worse than a Moore machine? What would the next floor signal be used for?

### Documentation
None.