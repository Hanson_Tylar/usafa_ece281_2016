--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   18:01:28 03/08/2016
-- Design Name:   
-- Module Name:   C:/Users/C18Tylar.Hanson/Documents/Spring_2016/ECE_281/Xilinx_Projects/CE3_Hanson/Moore_tb_Hanson.vhd
-- Project Name:  CE3_Hanson
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: MooreElevatorController_Shell
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY Moore_tb_Hanson IS
END Moore_tb_Hanson;
 
ARCHITECTURE behavior OF Moore_tb_Hanson IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT MooreElevatorController_Shell
    PORT(
         clk : IN  std_logic;
         reset : IN  std_logic;
         stop : IN  std_logic;
         up_down : IN  std_logic;
         floor : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';
   signal stop : std_logic := '0';
   signal up_down : std_logic := '0';

 	--Outputs
   signal floor : std_logic_vector(3 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: MooreElevatorController_Shell PORT MAP (
          clk => clk,
          reset => reset,
          stop => stop,
          up_down => up_down,
          floor => floor
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.	
      wait for clk_period*10;

      -- insert stimulus here 
		-- go to floor1
		reset <= '1'; stop <= '0'; up_down <= '0';
		wait for clk_period*5;
		ASSERT floor = "0001" REPORT "TEST 001 Failed." SEVERITY WARNING;
		
		-- go to floor2
		reset <= '0'; stop <= '0'; up_down <= '1';
		wait for clk_period;
		ASSERT floor = "0010" REPORT "TEST 002 Failed." SEVERITY WARNING;

		-- stop for five cycles
		reset <= '0'; stop <= '1'; up_down <= '1';
		wait for clk_period*5;
		ASSERT floor = "0010" REPORT "TEST 003 Failed." SEVERITY WARNING;

		-- go to floor3
		reset <= '0'; stop <= '0'; up_down <= '1';
		wait for clk_period;
		ASSERT floor = "0011" REPORT "TEST 004 Failed." SEVERITY WARNING;

		-- wait for five cycles
		reset <= '0'; stop <= '1'; up_down <= '1';
		wait for clk_period*5;
		ASSERT floor = "0011" REPORT "TEST 005 Failed." SEVERITY WARNING;

		-- go to floor4
		reset <= '0'; stop <= '0'; up_down <= '1';
		wait for clk_period;
		ASSERT floor = "0100" REPORT "TEST 006 Failed." SEVERITY WARNING;
		
		-- wait for five cycles
		reset <= '0'; stop <= '1'; up_down <= '1';
		wait for clk_period*5;
		ASSERT floor = "0100" REPORT "TEST 007 Failed." SEVERITY WARNING;

		-- go to floor3
		reset <= '0'; stop <= '0'; up_down <= '0';
		wait for clk_period;
		ASSERT floor = "0011" REPORT "TEST 008 Failed." SEVERITY WARNING;

		-- go to floor2
		reset <= '0'; stop <= '0'; up_down <= '0';
		wait for clk_period;
		ASSERT floor = "0010" REPORT "TEST 009 Failed." SEVERITY WARNING;

		-- go to floor1
		reset <= '0'; stop <= '0'; up_down <= '0';
		wait for clk_period;
		ASSERT floor = "0001" REPORT "TEST 010 Failed." SEVERITY WARNING;

      wait;
   end process;

END;
