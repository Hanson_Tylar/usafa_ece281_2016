<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_2" />
        <signal name="XLXN_3" />
        <signal name="XLXN_4" />
        <signal name="XLXN_5" />
        <signal name="XLXN_6" />
        <signal name="XLXN_7" />
        <signal name="clk" />
        <signal name="reset" />
        <signal name="left" />
        <signal name="right" />
        <signal name="hazard" />
        <signal name="lights_left(2:0)" />
        <signal name="lights_right(2:0)" />
        <port polarity="Input" name="clk" />
        <port polarity="Input" name="reset" />
        <port polarity="Input" name="left" />
        <port polarity="Input" name="right" />
        <port polarity="Input" name="hazard" />
        <port polarity="Output" name="lights_left(2:0)" />
        <port polarity="Output" name="lights_right(2:0)" />
        <blockdef name="next_state_logic">
            <timestamp>2016-2-29T20:27:35</timestamp>
            <rect width="256" x="64" y="-448" height="448" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-416" y2="-416" x1="320" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="output_logic">
            <timestamp>2016-2-29T20:27:25</timestamp>
            <rect width="256" x="64" y="-192" height="192" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-172" height="24" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="fd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <rect width="256" x="64" y="-320" height="256" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
        </blockdef>
        <block symbolname="next_state_logic" name="XLXI_1">
            <blockpin signalname="XLXN_2" name="s2" />
            <blockpin signalname="XLXN_3" name="s1" />
            <blockpin signalname="XLXN_4" name="s0" />
            <blockpin signalname="reset" name="reset" />
            <blockpin signalname="left" name="L" />
            <blockpin signalname="right" name="R" />
            <blockpin signalname="hazard" name="H" />
            <blockpin signalname="XLXN_5" name="s2_next" />
            <blockpin signalname="XLXN_6" name="s1_next" />
            <blockpin signalname="XLXN_7" name="s0_next" />
        </block>
        <block symbolname="output_logic" name="XLXI_2">
            <blockpin signalname="XLXN_2" name="s2" />
            <blockpin signalname="XLXN_3" name="s1" />
            <blockpin signalname="XLXN_4" name="s0" />
            <blockpin signalname="lights_left(2:0)" name="lights_L(2:0)" />
            <blockpin signalname="lights_right(2:0)" name="lights_R(2:0)" />
        </block>
        <block symbolname="fd" name="XLXI_3">
            <blockpin signalname="clk" name="C" />
            <blockpin signalname="XLXN_5" name="D" />
            <blockpin signalname="XLXN_2" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_4">
            <blockpin signalname="clk" name="C" />
            <blockpin signalname="XLXN_6" name="D" />
            <blockpin signalname="XLXN_3" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_5">
            <blockpin signalname="clk" name="C" />
            <blockpin signalname="XLXN_7" name="D" />
            <blockpin signalname="XLXN_4" name="Q" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="512" y="928" name="XLXI_1" orien="R0">
        </instance>
        <instance x="1792" y="800" name="XLXI_2" orien="R0">
        </instance>
        <instance x="1136" y="384" name="XLXI_3" orien="R0" />
        <instance x="1136" y="816" name="XLXI_4" orien="R0" />
        <instance x="1136" y="1232" name="XLXI_5" orien="R0" />
        <branch name="XLXN_2">
            <wire x2="432" y1="416" y2="512" x1="432" />
            <wire x2="512" y1="512" y2="512" x1="432" />
            <wire x2="1600" y1="416" y2="416" x1="432" />
            <wire x2="1600" y1="128" y2="128" x1="1520" />
            <wire x2="1600" y1="128" y2="416" x1="1600" />
            <wire x2="1696" y1="128" y2="128" x1="1600" />
            <wire x2="1696" y1="128" y2="640" x1="1696" />
            <wire x2="1792" y1="640" y2="640" x1="1696" />
        </branch>
        <branch name="XLXN_3">
            <wire x2="448" y1="432" y2="576" x1="448" />
            <wire x2="512" y1="576" y2="576" x1="448" />
            <wire x2="1600" y1="432" y2="432" x1="448" />
            <wire x2="1600" y1="432" y2="560" x1="1600" />
            <wire x2="1600" y1="560" y2="704" x1="1600" />
            <wire x2="1792" y1="704" y2="704" x1="1600" />
            <wire x2="1600" y1="560" y2="560" x1="1520" />
        </branch>
        <branch name="XLXN_4">
            <wire x2="496" y1="448" y2="640" x1="496" />
            <wire x2="512" y1="640" y2="640" x1="496" />
            <wire x2="1584" y1="448" y2="448" x1="496" />
            <wire x2="1584" y1="448" y2="976" x1="1584" />
            <wire x2="1680" y1="976" y2="976" x1="1584" />
            <wire x2="1584" y1="976" y2="976" x1="1520" />
            <wire x2="1680" y1="768" y2="976" x1="1680" />
            <wire x2="1792" y1="768" y2="768" x1="1680" />
        </branch>
        <branch name="XLXN_5">
            <wire x2="1008" y1="512" y2="512" x1="896" />
            <wire x2="1008" y1="128" y2="512" x1="1008" />
            <wire x2="1136" y1="128" y2="128" x1="1008" />
        </branch>
        <branch name="XLXN_6">
            <wire x2="1008" y1="704" y2="704" x1="896" />
            <wire x2="1008" y1="560" y2="704" x1="1008" />
            <wire x2="1136" y1="560" y2="560" x1="1008" />
        </branch>
        <branch name="XLXN_7">
            <wire x2="1008" y1="896" y2="896" x1="896" />
            <wire x2="1008" y1="896" y2="976" x1="1008" />
            <wire x2="1136" y1="976" y2="976" x1="1008" />
        </branch>
        <branch name="clk">
            <wire x2="1056" y1="256" y2="256" x1="432" />
            <wire x2="1120" y1="256" y2="256" x1="1056" />
            <wire x2="1136" y1="256" y2="256" x1="1120" />
            <wire x2="1056" y1="256" y2="688" x1="1056" />
            <wire x2="1136" y1="688" y2="688" x1="1056" />
            <wire x2="1056" y1="688" y2="1104" x1="1056" />
            <wire x2="1136" y1="1104" y2="1104" x1="1056" />
        </branch>
        <iomarker fontsize="28" x="432" y="256" name="clk" orien="R180" />
        <branch name="reset">
            <wire x2="496" y1="704" y2="704" x1="464" />
            <wire x2="512" y1="704" y2="704" x1="496" />
        </branch>
        <branch name="left">
            <wire x2="496" y1="768" y2="768" x1="432" />
            <wire x2="512" y1="768" y2="768" x1="496" />
        </branch>
        <branch name="right">
            <wire x2="496" y1="832" y2="832" x1="448" />
            <wire x2="512" y1="832" y2="832" x1="496" />
        </branch>
        <branch name="hazard">
            <wire x2="512" y1="896" y2="896" x1="480" />
        </branch>
        <iomarker fontsize="28" x="480" y="896" name="hazard" orien="R180" />
        <iomarker fontsize="28" x="448" y="832" name="right" orien="R180" />
        <iomarker fontsize="28" x="432" y="768" name="left" orien="R180" />
        <iomarker fontsize="28" x="464" y="704" name="reset" orien="R180" />
        <branch name="lights_left(2:0)">
            <wire x2="2208" y1="640" y2="640" x1="2176" />
        </branch>
        <iomarker fontsize="28" x="2208" y="640" name="lights_left(2:0)" orien="R0" />
        <branch name="lights_right(2:0)">
            <wire x2="2192" y1="768" y2="768" x1="2176" />
            <wire x2="2208" y1="768" y2="768" x1="2192" />
        </branch>
        <iomarker fontsize="28" x="2208" y="768" name="lights_right(2:0)" orien="R0" />
    </sheet>
</drawing>