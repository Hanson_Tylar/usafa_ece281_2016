<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="Led(5:0)" />
        <signal name="sw1" />
        <signal name="sw7" />
        <signal name="sw5" />
        <signal name="sw6" />
        <signal name="Led(2:0)" />
        <signal name="XLXN_10" />
        <signal name="clk" />
        <signal name="sw0" />
        <signal name="Led(5:3)" />
        <port polarity="Output" name="Led(5:0)" />
        <port polarity="Input" name="sw1" />
        <port polarity="Input" name="sw7" />
        <port polarity="Input" name="sw5" />
        <port polarity="Input" name="sw6" />
        <port polarity="Input" name="clk" />
        <port polarity="Input" name="sw0" />
        <blockdef name="thunderbird_fsm">
            <timestamp>2016-2-29T22:6:4</timestamp>
            <rect width="256" x="64" y="-320" height="320" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-300" height="24" />
            <line x2="384" y1="-288" y2="-288" x1="320" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="clock_divider">
            <timestamp>2016-2-29T22:7:15</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <block symbolname="thunderbird_fsm" name="XLXI_1">
            <blockpin signalname="XLXN_10" name="clk" />
            <blockpin signalname="sw1" name="reset" />
            <blockpin signalname="sw7" name="left" />
            <blockpin signalname="sw5" name="right" />
            <blockpin signalname="sw6" name="hazard" />
            <blockpin signalname="Led(5:3)" name="lights_left(2:0)" />
            <blockpin signalname="Led(2:0)" name="lights_right(2:0)" />
        </block>
        <block symbolname="clock_divider" name="XLXI_2">
            <blockpin signalname="clk" name="clk_fast" />
            <blockpin signalname="sw0" name="reset" />
            <blockpin signalname="XLXN_10" name="clk_slow" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="560" y="416" name="XLXI_1" orien="R0">
        </instance>
        <branch name="Led(5:0)">
            <wire x2="1040" y1="240" y2="240" x1="1024" />
            <wire x2="1104" y1="240" y2="240" x1="1040" />
            <wire x2="1168" y1="240" y2="240" x1="1104" />
        </branch>
        <bustap x2="1104" y1="240" y2="336" x1="1104" />
        <branch name="sw1">
            <wire x2="560" y1="192" y2="192" x1="544" />
        </branch>
        <branch name="sw7">
            <wire x2="560" y1="256" y2="256" x1="544" />
        </branch>
        <branch name="sw5">
            <wire x2="560" y1="320" y2="320" x1="544" />
        </branch>
        <branch name="sw6">
            <wire x2="560" y1="384" y2="384" x1="544" />
        </branch>
        <branch name="Led(2:0)">
            <wire x2="1104" y1="384" y2="384" x1="944" />
            <wire x2="1104" y1="336" y2="384" x1="1104" />
        </branch>
        <branch name="XLXN_10">
            <wire x2="560" y1="128" y2="128" x1="512" />
        </branch>
        <branch name="clk">
            <wire x2="128" y1="128" y2="128" x1="112" />
        </branch>
        <branch name="sw0">
            <wire x2="128" y1="192" y2="192" x1="112" />
        </branch>
        <instance x="128" y="224" name="XLXI_2" orien="R0">
        </instance>
        <iomarker fontsize="28" x="1168" y="240" name="Led(5:0)" orien="R0" />
        <iomarker fontsize="28" x="544" y="192" name="sw1" orien="R180" />
        <iomarker fontsize="28" x="544" y="256" name="sw7" orien="R180" />
        <iomarker fontsize="28" x="544" y="320" name="sw5" orien="R180" />
        <iomarker fontsize="28" x="544" y="384" name="sw6" orien="R180" />
        <iomarker fontsize="28" x="112" y="128" name="clk" orien="R180" />
        <iomarker fontsize="28" x="112" y="192" name="sw0" orien="R180" />
        <bustap x2="1040" y1="240" y2="144" x1="1040" />
        <branch name="Led(5:3)">
            <wire x2="1040" y1="128" y2="128" x1="944" />
            <wire x2="1040" y1="128" y2="144" x1="1040" />
        </branch>
    </sheet>
</drawing>