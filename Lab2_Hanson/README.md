# Lab #2 - Thunderbird Turn Signal

## By C3C Hanson

## Table of Contents
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Preliminary Design](#preliminary-design)
3. [Hardware schematic](#hardware-schematic)
4. [Well-formatted code](#well-formatted-code)
5. [Debugging](#debugging)
6. [Testing methodology or results](#testing-methodology-or-results)
7. [Answers to Lab Questions](#answers-to-lab-questions)
8. [Observations and Conclusions](#observations-and-conclusions)
9. [Documentation](#documentation)
 
### Objectives or Purpose 
In this lab, I will design, write, test, and implement in hardware a finite state machine to simulate the taillights of a 1965 Ford Thunderbird.

### Preliminary design
The first step in designing a finite state machine to simulate the Thunderbird's taillights is to make a state diagram. Below is a state diagram of a Moore machine.

![](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/ca11f93f938ce55c4368d2153df761c0cac0f2bc/Lab2_Hanson/state_diagram.JPG?token=8aa6ec5a41ba3bb1cd100c0723420537fa7dbf03)

The next step is to create a next state table and output table from the state diagram.

![](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/ca11f93f938ce55c4368d2153df761c0cac0f2bc/Lab2_Hanson/next_state_and_output_tables.jpg?token=a99e970c8c9b18313610b1182b23e48d936b7a8f)

### Hardware schematic
My top schematic consists of a clock, reset, left, right, or hazard inputs, and six LEDs as the output.
![](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/ca11f93f938ce55c4368d2153df761c0cac0f2bc/Lab2_Hanson/top_sch.JPG?token=07404e1d97cb33ac107f969656c96859c82df85b)

Making up the finite state machine is three flip flops, next state logic, and output logic. The VHDL code for next state and output logic is shown in the next section.
![](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/ca11f93f938ce55c4368d2153df761c0cac0f2bc/Lab2_Hanson/thunderbird_fsm.JPG?token=c33085f3c9c2add7acfb26dd78818a873ad6dc90)

### Well-formatted code
The equations used for my next state logic and output logic were created from the next state and output tables shown above. 
The code shown below is the VHDL implementation of these Boolean equations.
  --------------------------------------------------------------------
	-- Name: Tylar Hanson
	-- Date: 28 Feb 2016
	-- Course: ECE 281
	-- File: next_state_logic.vhd
	-- Lab: Lab2 Thunderbird Turn Signal
	--
	-- Purp:In this lab, I will design, write, test, and implement in hardware 
	-- a finite state machine to simulate the taillights of a 1965 Ford Thunderbird. 
	--
	-- Doc:	<None>
	--
	-- Academic Integrity Statement: I certify that, while others may have 
	-- assisted me in brain storming, debugging and validating this program, 
	-- the program itself is my own work. I understand that submitting code 
	-- which is the work of other individuals is a violation of the honor   
	-- code.  I also understand that if I knowingly give my original work to 
	-- another individual is also a violation of the honor code. 
	------------------------------------------------------------------------- 
	library IEEE;
	use IEEE.STD_LOGIC_1164.ALL;

	-- Uncomment the following library declaration if using
	-- arithmetic functions with Signed or Unsigned values
	--use IEEE.NUMERIC_STD.ALL;

	-- Uncomment the following library declaration if instantiating
	-- any Xilinx primitives in this code.
	--library UNISIM;
	--use UNISIM.VComponents.all;

	entity next_state_logic is
	    Port ( s2 : in  STD_LOGIC;
	           s1 : in  STD_LOGIC;
	           s0 : in  STD_LOGIC;
	           reset : in  STD_LOGIC;
	           L : in  STD_LOGIC;
	           R : in  STD_LOGIC;
	           H : in  STD_LOGIC;
	           s2_next : out  STD_LOGIC;
	           s1_next : out  STD_LOGIC;
	           s0_next : out  STD_LOGIC);
	end next_state_logic;
	
	architecture Behavioral of next_state_logic is
	
	begin

	s2_next <= '1' when s2 = '0' and s1 = '0' and s0 = '0' and reset = '0' and H = '1' else
				  '1' when s2 = '0' and s1 = '0' and s0 = '0' and reset = '0' and L = '0' and R = '1' and H = '0' else
				  '1' when s2 = '1' and s1 = '0' and s0 = '0' and reset = '0' else
				  '1' when s2 = '1' and s1 = '0' and s0 = '1' and reset = '0' else
				  '0';
				  
	s1_next <= '1' when s2 = '0' and s1 = '0' and s0 = '0' and reset = '0' and H = '1' else
				  '1' when s2 = '0' and s1 = '0' and s0 = '1' and reset = '0' else
				  '1' when s2 = '0' and s1 = '1' and s0 = '0' and reset = '0' else
				  '1' when s2 = '1' and s1 = '0' and s0 = '1' and reset = '0' else
				  '0';
				  
	s0_next <= '1' when s2 = '0' and s1 = '0' and s0 = '0' and reset = '0' and H = '1' else
				  '1' when s2 = '0' and s1 = '0' and s0 = '0' and reset = '0' and L = '1' and H = '0' else
				  '1' when s2 = '0' and s1 = '1' and s0 = '0' and reset = '0' else
				  '1' when s2 = '1' and s1 = '0' and s0 = '0' and reset = '0' else
				  '0';
	
	end Behavioral;  

--------------------------------------------------------------------
	-- Name: Tylar Hanson
	-- Date: 28 Feb 2016
	-- Course: ECE 281
	-- File: output_logic.vhd
	-- Lab: Lab2 Thunderbird Turn Signal
	--
	-- Purp:In this lab, I will design, write, test, and implement in hardware 
	-- a finite state machine to simulate the taillights of a 1965 Ford Thunderbird. 
	--
	-- Doc:	<None>
	--
	-- Academic Integrity Statement: I certify that, while others may have 
	-- assisted me in brain storming, debugging and validating this program, 
	-- the program itself is my own work. I understand that submitting code 
	-- which is the work of other individuals is a violation of the honor   
	-- code.  I also understand that if I knowingly give my original work to 
	-- another individual is also a violation of the honor code. 
	------------------------------------------------------------------------- 
	library IEEE;
	use IEEE.STD_LOGIC_1164.ALL;
	
	-- Uncomment the following library declaration if using
	-- arithmetic functions with Signed or Unsigned values
	--use IEEE.NUMERIC_STD.ALL;
	
	-- Uncomment the following library declaration if instantiating
	-- any Xilinx primitives in this code.
	--library UNISIM;
	--use UNISIM.VComponents.all;
	
	entity output_logic is
	Port ( s2 : in  STD_LOGIC;
	   s1 : in  STD_LOGIC;
	   s0 : in  STD_LOGIC;
	   lights_L : out  STD_LOGIC_VECTOR (2 downto 0);
	   lights_R : out  STD_LOGIC_VECTOR (2 downto 0));
	end output_logic;
	
	architecture Behavioral of output_logic is
	
	begin

	lights_L <= "001" when s2 = '0' and s1 = '0' and s0 = '1' else
					"011" when s2 = '0' and s1 = '1' and s0 = '0' else
					"111" when s2 = '0' and s1 = '1' and s0 = '1' else
					"111" when s2 = '1' and s1 = '1' and s0 = '1' else
					"000";
					
	lights_R <= "100" when s2 = '1' and s1 = '0' and s0 = '0' else
					"110" when s2 = '1' and s1 = '0' and s0 = '1' else
					"111" when s2 = '1' and s1 = '1' and s0 = '0' else
					"111" when s2 = '1' and s1 = '1' and s0 = '1' else
					"000";
	
	end Behavioral;

### Debugging
I encountered an error while writing my ECE281_Lab2.ucf file. After troubleshooting I realized I included a statement for 'sw2' when this switch is not used in my design. Another error I had was in my next_state_logic. I had a typo in one of the assignment statements. After correcting these my design worked properly. 

### Testing methodology or results

##### Software Testing
When simulating my software the following cases were tested: 
Signal left for two cycles, signal left for two cycles with the signal released, signal right for two cycles, signal right for two cycles with the signal released, hazards for three cycles, signal left with reset, signal right with reset, and signal left with left and right signals high.

![](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/637ee14110c62aceb206c9c6ceb56b484e012821/Lab2_Hanson/ISIM_capture.JPG?token=5e4c6ea5d4015c8e46f17177ae0cb8deb9003b31)

Comparing the resulting wave forms with the expected results I knew that my machine is behaving as designed. With this working I moved onto a hardware implementation. 

##### Hardware Testing
To test the hardware I tested the left, right, and hazard signals each on their own for several cycles. When the lights responded as they were designed to I then tested each signal with a reset signal given in mid-cycle. The left and right turn signals returned to the zero, all off, state before finishing their cycles as desired.The left and right cycles were also tested with the signal released mid cycle.

### Answers to Lab Questions
1. I designed a Moore Machine.
2. Explain how the clock divider works and draw a block diagram.
	1. A VHDL entity is a declaration of a module's inputs and outputs.
	2. a VHDL architecture is a description of the module's internal behavior or structure.
	3. The clock divider takes as inputs a clock and a reset signal. A 24 bit variable clk_bus is assigned and is set to zero whenever the reset signal is applied. If not in a reset state the variable is incremented by binary 1 on each rising edge of the input clock. The output clock will be given the value of the most significant bit of the variable clk_bus. The output clock will have one rising edge for every 2^23 rising edges on the input clock.
   Below is a block diagram of the clock divider.

![](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/cf2810ede68a4a9c641370da0e640674962a59bf/Lab2_Hanson/clk_divider_diagram.JPG?token=c3f8d6fbdf44dc42617bd3a095abd154051ccf95)

3. When the clock reset is applied the clock signal is set to zero and held there until the reset is released. When this happens there is no rising edge for my FSM to transition on. So it's almost like a pause button, the machine just sits in it's current state. When the FSM reset is applied the current state is set to the idle, zero, state and held there until the FSM reset signal is released. 

### Observations and Conclusions
The purpose of this lab was to design, write, test, and implement in hardware a finite state machine to simulate the taillights of a 1965 Ford Thunderbird. I think that using VHDL to create the bottom level sources is a good follow on from the intro to VHDL we had in class a few lessons ago. I learned to use the design summary page to track down errors in my design, it was very helpful The grade sheet was collected before I filled in the hours spent on this assignment. It should say about six hours.

### Documentation
Captain Falkinburg showed me how to tie my lights_L and lights_R buses together with bus taps and make a single output from this bus. 