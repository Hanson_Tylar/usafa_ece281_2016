-- Vhdl test bench created from schematic C:\Users\C18Tylar.Hanson\Documents\Spring_2016\ECE_281\Xilinx_Projects\Lab2_Hanson\thunderbird_fsm.sch - Mon Feb 29 15:02:36 2016
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY thunderbird_fsm_thunderbird_fsm_sch_tb IS
END thunderbird_fsm_thunderbird_fsm_sch_tb;
ARCHITECTURE behavioral OF thunderbird_fsm_thunderbird_fsm_sch_tb IS 

   COMPONENT thunderbird_fsm
   PORT( clk	:	IN	STD_LOGIC; 
          reset	:	IN	STD_LOGIC; 
          left	:	IN	STD_LOGIC; 
          right	:	IN	STD_LOGIC; 
          hazard	:	IN	STD_LOGIC; 
          lights_left	:	OUT	STD_LOGIC_VECTOR (2 DOWNTO 0); 
          lights_right	:	OUT	STD_LOGIC_VECTOR (2 DOWNTO 0));
   END COMPONENT;

   SIGNAL clk	:	STD_LOGIC;
   SIGNAL reset	:	STD_LOGIC;
   SIGNAL left	:	STD_LOGIC;
   SIGNAL right	:	STD_LOGIC;
   SIGNAL hazard	:	STD_LOGIC;
   SIGNAL lights_left	:	STD_LOGIC_VECTOR (2 DOWNTO 0);
   SIGNAL lights_right	:	STD_LOGIC_VECTOR (2 DOWNTO 0);

BEGIN

   UUT: thunderbird_fsm PORT MAP(
		clk => clk, 
		reset => reset, 
		left => left, 
		right => right, 
		hazard => hazard, 
		lights_left => lights_left, 
		lights_right => lights_right
   );
process
	begin
		clk <= '0';
		wait for 5 ns;
		clk <= '1';
		wait for 5 ns;
	end process;

-- *** Test Bench - User Defined Section ***

   tb : PROCESS
   BEGIN
		
		-- Reset (state 0)
		reset <= '1'; left <= '0'; right <= '0'; hazard <= '0';
		wait for 10 ns;
		
		-- Signal left two cycles
		reset <= '0'; left <= '1'; right <= '0'; hazard <= '0';
		wait for 80 ns;
		
		reset <= '0'; left <= '0'; right <= '0'; hazard <= '0';
		wait for 40 ns;

		-- Signal left two cycles with signal released
		reset <= '0'; left <= '1'; right <= '0'; hazard <= '0';
		wait for 50 ns;
		
		reset <= '0'; left <= '0'; right <= '0'; hazard <= '0';
		wait for 40 ns;
		
		-- Signal right two cycles
		reset <= '0'; left <= '0'; right <= '1'; hazard <= '0';
		wait for 80 ns;
		
		reset <= '0'; left <= '0'; right <= '0'; hazard <= '0';
		wait for 40 ns;

		-- Signal right two cycles with signal released
		reset <= '0'; left <= '0'; right <= '1'; hazard <= '0';
		wait for 50 ns;
		
		reset <= '0'; left <= '0'; right <= '0'; hazard <= '0';
		wait for 40 ns;
		
		-- Hazards for three cycles
		reset <= '0'; left <= '0'; right <= '0'; hazard <= '1';
		wait for 60 ns;
		
		reset <= '0'; left <= '0'; right <= '0'; hazard <= '0';
		wait for 40 ns;
		
		-- Signal left with reset
		reset <= '0'; left <= '1'; right <= '0'; hazard <= '0';
		wait for 20 ns;
		
		reset <= '1'; left <= '1'; right <= '0'; hazard <= '0';
		wait for 40 ns;
		
		-- Signal left with reset
		reset <= '0'; left <= '0'; right <= '1'; hazard <= '0';
		wait for 10 ns;
		
		reset <= '1'; left <= '0'; right <= '1'; hazard <= '0';
		wait for 40 ns;
		
		-- Left and right signal will do left turn signal
		reset <= '0'; left <= '1'; right <= '1'; hazard <= '0';
		wait for 80 ns;
		
		reset <= '1'; left <= '1'; right <= '1'; hazard <= '0';
		wait for 40 ns;
		
		WAIT;
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
