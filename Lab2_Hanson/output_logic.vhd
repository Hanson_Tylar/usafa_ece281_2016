----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:08:40 02/29/2016 
-- Design Name: 
-- Module Name:    output_logic - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
--------------------------------------------------------------------
	-- Name: Tylar Hanson
	-- Date: 28 Feb 2016
	-- Course: ECE 281
	-- File: output_logic.vhd
	-- Lab: Lab2 Thunderbird Turn Signal
	--
	-- Purp:In this lab, I will design, write, test, and implement in hardware 
	-- a finite state machine to simulate the taillights of a 1965 Ford Thunderbird. 
	--
	-- Doc:	<list the names of the people who you helped>
	-- 
	--
	-- Academic Integrity Statement: I certify that, while others may have 
	-- assisted me in brain storming, debugging and validating this program, 
	-- the program itself is my own work. I understand that submitting code 
	-- which is the work of other individuals is a violation of the honor   
	-- code.  I also understand that if I knowingly give my original work to 
	-- another individual is also a violation of the honor code. 
	------------------------------------------------------------------------- 
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity output_logic is
    Port ( s2 : in  STD_LOGIC;
           s1 : in  STD_LOGIC;
           s0 : in  STD_LOGIC;
           lights_L : out  STD_LOGIC_VECTOR (2 downto 0);
           lights_R : out  STD_LOGIC_VECTOR (2 downto 0));
end output_logic;

architecture Behavioral of output_logic is

begin

lights_L <= "001" when s2 = '0' and s1 = '0' and s0 = '1' else
				"011" when s2 = '0' and s1 = '1' and s0 = '0' else
				"111" when s2 = '0' and s1 = '1' and s0 = '1' else
				"111" when s2 = '1' and s1 = '1' and s0 = '1' else
				"000";
				
lights_R <= "100" when s2 = '1' and s1 = '0' and s0 = '0' else
				"110" when s2 = '1' and s1 = '0' and s0 = '1' else
				"111" when s2 = '1' and s1 = '1' and s0 = '0' else
				"111" when s2 = '1' and s1 = '1' and s0 = '1' else
				"000";

end Behavioral;

