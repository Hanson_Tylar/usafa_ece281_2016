#--------------------------------------------------------------------
#-- Name:	Tylar Hanson 
#-- Date:	18 April 2016
#-- Course:	ECE_281
#-- File:	MIPS_ICEPart2.asm
#-- HW:		MIPS ICE
#--
#-- Purp:	This program counts takes the 2s complement of a number
#--		given by user input.  
#--
#-- Documentation:  None
#--
#-- Academic Integrity Statement: I certify that, while others may have 
#-- assisted me in brain storming, debugging and validating this program, 
#-- the program itself is my own work. I understand that submitting code 
#-- which is the work of other individuals is a violation of the honor   
#-- code.  I also understand that if I knowingly give my original work to 
#-- another individual is also a violation of the honor code. 
#------------------------------------------------------------------------- 

.text
main:
	# Prompt for the integer to enter
	addi $2, $0, 0x00000004
	addi $s0, $0, 0xFFFFFFFF
	#li $v0, 4
	lui $1, 0x00001001
	ori $4, $1, 0x00000000
	#la $a0, prompt 
	syscall
	
	# Read the integer and save it in $s1
	# li $v0, 5
	addi $2, $0, 0x00000005
	syscall
	move $s1, $v0
	
	# Take two's complement of the number
	xor $s1, $s1, $s0
	addi $s1, $s1, 0x00000001

	#Print the new number
	addi $2, $0, 0x00000001
	move $a0, $s1
	syscall

done:
	j done
.data
prompt: .asciiz "Please enter an integer: "
