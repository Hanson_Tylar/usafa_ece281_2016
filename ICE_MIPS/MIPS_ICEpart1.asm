#--------------------------------------------------------------------
#-- Name:	Tylar Hanson 
#-- Date:	18 April 2016
#-- Course:	ECE_281
#-- File:	MIPS_ICEPart1.asm
#-- HW:		MIPS ICE
#--
#-- Purp:	This program counts up and down by 2s between 0 and 10  
#--
#-- Documentation:  None
#--
#-- Academic Integrity Statement: I certify that, while others may have 
#-- assisted me in brain storming, debugging and validating this program, 
#-- the program itself is my own work. I understand that submitting code 
#-- which is the work of other individuals is a violation of the honor   
#-- code.  I also understand that if I knowingly give my original work to 
#-- another individual is also a violation of the honor code. 
#------------------------------------------------------------------------- 	
#	Part 1 questions before part e.
#	a) This program initializes two values and adds four to the second until it equals the first value
#	b) Comments added to each line
#	c) The loop runs 5 times
#	d) The final value of $t0 is 0x2d


	addi $t0, $0, 0 #  $t0 = 0
	addi $t1, $0, 10 # $t1 = 10
loop1:	
	addi $t0, $t0, 2 # $t0 += 2
	beq $t0, $t1, loop2 # if $t0 == $t1 go to loop2
	j loop1 # else go to loop1 
loop2:	
	addi $t0, $t0, -2 # $t2 += -2
	beq $t0, $s0, loop1 # if $t2 == 0 go to loop1
	j loop2 # else go to loop2
