-- Vhdl test bench created from schematic C:\Users\C18Tylar.Hanson\Documents\Spring_2016\ECE 281\Xilinx Projects\CE1_Hanson\CE1_Hanson_top.sch - Wed Jan 13 23:26:45 2016
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY CE1_Hanson_top_CE1_Hanson_top_sch_tb IS
END CE1_Hanson_top_CE1_Hanson_top_sch_tb;
ARCHITECTURE behavioral OF CE1_Hanson_top_CE1_Hanson_top_sch_tb IS 

   COMPONENT CE1_Hanson_top
   PORT( sw7 : IN STD_LOGIC;
			sw6 : IN STD_LOGIC;
			sw5 : IN STD_LOGIC;
			led0 : OUT STD_LOGIC );
   END COMPONENT;

	SIGNAL sw7 : STD_LOGIC;
	SIGNAL sw6 : STD_LOGIC;
	SIGNAL sw5 : STD_LOGIC;
	SIGNAL led0 : STD_LOGIC;
BEGIN

   UUT: CE1_Hanson_top PORT MAP(
		sw7 => sw7,
		sw6 => sw6,
		sw5 => sw5,
		led0 => led0
   );

-- *** Test Bench - User Defined Section ***
   tb : PROCESS 
   BEGIN
	
		sw7 <= '0'; sw6 <= '0'; sw5 <= '0';
		wait for 10 ns;
		
		sw7 <= '0'; sw6 <= '0'; sw5 <= '1';
		wait for 10 ns;
		
		sw7 <= '0'; sw6 <= '1'; sw5 <= '0';
		wait for 10 ns;
		
		sw7 <= '0'; sw6 <= '1'; sw5 <= '1';
		wait for 10 ns;
		
		sw7 <= '1'; sw6 <= '0'; sw5 <= '0';
		wait for 10 ns;
		
		sw7 <= '1'; sw6 <= '0'; sw5 <= '1';
		wait for 10 ns;
		
		sw7 <= '1'; sw6 <= '1'; sw5 <= '0';
		wait for 10 ns;
		
		sw7 <= '1'; sw6 <= '1'; sw5 <= '1';
		wait for 10 ns;
		
      WAIT; -- will wait forever
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
