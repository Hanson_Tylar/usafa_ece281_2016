#Computer Exercise #1 - Intro to Xilinx and Digital Logic

## By C3C Tylar Hanson

## Table of Contents
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Preliminary Design](#preliminary-design)
3. [Software flow chart or algorithms](#software-flow-chart-or-algorithms)
4. [Hardware schematic](#hardware-schematic)
5. [Well-formatted code](#well-formatted-code)
6. [Debugging](#debugging)
7. [Testing methodology or results](#testing-methodology-or-results)
8. [Observations and Conclusions](#observations-and-conclusions)
9. [Documentation](#documentation)
 
### Objectives or Purpose 
In this computer exercise I will install the Xilinx development software and create, test, and implement a design that uses simple logic to illuminate a light based on certain switch inputs to the Nexys2 Development Board.  

### Preliminary design

#### AB' + BC =F
| A | B | C | expected F | sim F |
|:-:|:-:|:-:|:-:|:-:|
| 0 | 0 | 0 | 0 | 0 |
| 0 | 0 | 1 | 0 | 0 |
| 0 | 1 | 0 | 0 | 0 |
| 0 | 1 | 1 | 1 | 1 |
| 1 | 0 | 0 | 1 | 1 |
| 1 | 0 | 1 | 1 | 1 |
| 1 | 1 | 0 | 0 | 0 |
| 1 | 1 | 1 | 1 | 1 |


### Hardware schematic
Hardware schematic can be found in my BitBucket

### Well-formatted code

When I generated the test bench I had to code the port map and 
the cases used for testing.

    COMPONENT CE1_Hanson_top
       PORT( 	sw7 : IN STD_LOGIC;
    			sw6 : IN STD_LOGIC;
    			sw5 : IN STD_LOGIC;
    			led0 : OUT STD_LOGIC );
       END COMPONENT;
    
    	SIGNAL sw7 : STD_LOGIC;
    	SIGNAL sw6 : STD_LOGIC;
    	SIGNAL sw5 : STD_LOGIC;
    	SIGNAL led0 : STD_LOGIC;
    BEGIN
    
       UUT: CE1_Hanson_top PORT MAP(
    		sw7 => sw7,
    		sw6 => sw6,
    		sw5 => sw5,
    		led0 => led0
       );
    
    -- *** Test Bench - User Defined Section ***
       tb : PROCESS 
       BEGIN
    	
    		sw7 <= '0'; sw6 <= '0'; sw5 <= '0';
    		wait for 10 ns;
    		
    		sw7 <= '0'; sw6 <= '0'; sw5 <= '1';
    		wait for 10 ns;
    		
    		sw7 <= '0'; sw6 <= '1'; sw5 <= '0';
    		wait for 10 ns;
    		
    		sw7 <= '0'; sw6 <= '1'; sw5 <= '1';
    		wait for 10 ns;
    		
    		sw7 <= '1'; sw6 <= '0'; sw5 <= '0';
    		wait for 10 ns;
    		
    		sw7 <= '1'; sw6 <= '0'; sw5 <= '1';
    		wait for 10 ns;
    		
    		sw7 <= '1'; sw6 <= '1'; sw5 <= '0';
    		wait for 10 ns;
    		
    		sw7 <= '1'; sw6 <= '1'; sw5 <= '1';
    		wait for 10 ns;
    

### Debugging
When first trying to simulate the test bench I kept getting a bunch of errors. Eventually I was able to figure out that it was because I didn't have the four signal code lines above. 

### Testing methodology or results
In my bit bucket repository you can find the build schematic and the simulation results screen shot. 

### Observations and Conclusions
The purpose of this lab was to learn how to use the Xilinx software and become familiar with git. I have gained a better understanding of how to use these programs for ECE labs. I've also come to the conclusion that for the next lab I need to take better notes along the way.

### Documentation
None