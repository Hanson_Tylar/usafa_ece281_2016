--------------------------------------------------------------------
-- Name: Tylar Hanson
-- Date: 09 April 2016
-- Course:	ECE_281
-- File: alu_tb.vhd
-- HW:	CE #4 32-Bit ALU
--
-- Purp: Design, write, and test a 32-bit Arithmetic Logic Unit (ALU)
--
-- Doc:	None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
ENTITY alu_tb IS
END alu_tb;
 
ARCHITECTURE behavior OF alu_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT alu
    PORT(
         a : IN  std_logic_vector(31 downto 0);
         b : IN  std_logic_vector(31 downto 0);
         f : IN  std_logic_vector(2 downto 0);
         y : OUT  std_logic_vector(31 downto 0);
         zero : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal a : std_logic_vector(31 downto 0) := (others => '0');
   signal b : std_logic_vector(31 downto 0) := (others => '0');
   signal f : std_logic_vector(2 downto 0) := (others => '0');

 	--Outputs
   signal y : std_logic_vector(31 downto 0);
   signal zero : std_logic;


BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: alu PORT MAP (
          a => a,
          b => b,
          f => f,
          y => y,
          zero => zero
        );
 
   -- Stimulus process
   stim_proc: process
   begin		

		f <= "010"; a <= x"00000000"; b <= x"00000000";
		wait for 10ns;
		ASSERT y = x"00000000" REPORT "TEST 001.0 FAILED" SEVERITY ERROR;
		ASSERT zero = '1' REPORT "TEST 001.1 FAILED" SEVERITY ERROR;
		
		f <= "010"; a <= x"00000000"; b <= x"FFFFFFFF";
		wait for 10ns;
		ASSERT y = x"FFFFFFFF" REPORT "TEST 002.0 FAILED" SEVERITY ERROR;
		ASSERT zero = '0' REPORT "TEST 002.1 FAILED" SEVERITY ERROR;
		
		f <= "010"; a <= x"00000001"; b <= x"FFFFFFFF";
		wait for 10ns;
		ASSERT y = x"00000000" REPORT "TEST 003.0 FAILED" SEVERITY ERROR;
		ASSERT zero = '1' REPORT "TEST 003.1 FAILED" SEVERITY ERROR;
		
		f <= "010"; a <= x"000000FF"; b <= x"00000001";
		wait for 10ns;
		ASSERT y = x"00000100" REPORT "TEST 004.0 FAILED" SEVERITY ERROR;
		ASSERT zero = '0' REPORT "TEST 004.1 FAILED" SEVERITY ERROR;
		
		f <= "110"; a <= x"00000000"; b <= x"00000000";
		wait for 10ns;
		ASSERT y = x"00000000" REPORT "TEST 005.0 FAILED" SEVERITY ERROR;
		ASSERT zero = '1' REPORT "TEST 005.1 FAILED" SEVERITY ERROR;
		
		f <= "110"; a <= x"00000000"; b <= x"FFFFFFFF";
		wait for 10ns;
		ASSERT y = x"00000001" REPORT "TEST 006.0 FAILED" SEVERITY ERROR;
		ASSERT zero = '0' REPORT "TEST 006.1 FAILED" SEVERITY ERROR;
		
		f <= "110"; a <= x"00000001"; b <= x"00000001";
		wait for 10ns;
		ASSERT y = x"00000000" REPORT "TEST 007.0 FAILED" SEVERITY ERROR;
		ASSERT zero = '1' REPORT "TEST 007.1 FAILED" SEVERITY ERROR;
		
		f <= "110"; a <= x"00000100"; b <= x"00000001";
		wait for 10ns;
		ASSERT y = x"000000FF" REPORT "TEST 008.0 FAILED" SEVERITY ERROR;
		ASSERT zero = '0' REPORT "TEST 008.1 FAILED" SEVERITY ERROR;
		
		f <= "111"; a <= x"00000000"; b <= x"00000000";
		wait for 10ns;
		ASSERT y = x"00000000" REPORT "TEST 009.0 FAILED" SEVERITY ERROR;
		ASSERT zero = '1' REPORT "TEST 009.1 FAILED" SEVERITY ERROR;
		
		f <= "111"; a <= x"00000000"; b <= x"00000001";
		wait for 10ns;
		ASSERT y = x"00000001" REPORT "TEST 010.0 FAILED" SEVERITY ERROR;
		ASSERT zero = '0' REPORT "TEST 010.1 FAILED" SEVERITY ERROR;
		
		f <= "111"; a <= x"00000000"; b <= x"FFFFFFFF";
		wait for 10ns;
		ASSERT y = x"00000000" REPORT "TEST 011.0 FAILED" SEVERITY ERROR;
		ASSERT zero = '1' REPORT "TEST 011.1 FAILED" SEVERITY ERROR;
		
		f <= "111"; a <= x"00000001"; b <= x"00000000";
		wait for 10ns;
		ASSERT y = x"00000000" REPORT "TEST 012.0 FAILED" SEVERITY ERROR;
		ASSERT zero = '1' REPORT "TEST 012.1 FAILED" SEVERITY ERROR;
		
		f <= "111"; a <= x"FFFFFFFF"; b <= x"00000000";
		wait for 10ns;
		ASSERT y = x"00000001" REPORT "TEST 013.0 FAILED" SEVERITY ERROR;
		ASSERT zero = '0' REPORT "TEST 013.1 FAILED" SEVERITY ERROR;
		
		f <= "000"; a <= x"FFFFFFFF"; b <= x"FFFFFFFF";
		wait for 10ns;
		ASSERT y = x"FFFFFFFF" REPORT "TEST 014.0 FAILED" SEVERITY ERROR;
		ASSERT zero = '0' REPORT "TEST 014.1 FAILED" SEVERITY ERROR;
		
		f <= "000"; a <= x"FFFFFFFF"; b <= x"12345678";
		wait for 10ns;
		ASSERT y = x"12345678" REPORT "TEST 015.0 FAILED" SEVERITY ERROR;
		ASSERT zero = '0' REPORT "TEST 015.1 FAILED" SEVERITY ERROR;
		
		f <= "000"; a <= x"12345678"; b <= x"87654321";
		wait for 10ns;
		ASSERT y = x"02244220" REPORT "TEST 016.0 FAILED" SEVERITY ERROR;
		ASSERT zero = '0' REPORT "TEST 016.1 FAILED" SEVERITY ERROR;
		
		f <= "000"; a <= x"00000000"; b <= x"FFFFFFFF";
		wait for 10ns;
		ASSERT y = x"00000000" REPORT "TEST 017.0 FAILED" SEVERITY ERROR;
		ASSERT zero = '1' REPORT "TEST 017.1 FAILED" SEVERITY ERROR;
		
		f <= "001"; a <= x"FFFFFFFF"; b <= x"FFFFFFFF";
		wait for 10ns;
		ASSERT y = x"FFFFFFFF" REPORT "TEST 018.0 FAILED" SEVERITY ERROR;
		ASSERT zero = '0' REPORT "TEST 018.1 FAILED" SEVERITY ERROR;
		
		f <= "001"; a <= x"12345678"; b <= x"87654321";
		wait for 10ns;
		ASSERT y = x"97755779" REPORT "TEST 019.0 FAILED" SEVERITY ERROR;
		ASSERT zero = '0' REPORT "TEST 019.1 FAILED" SEVERITY ERROR;
		
		f <= "001"; a <= x"00000000"; b <= x"FFFFFFFF";
		wait for 10ns;
		ASSERT y = x"FFFFFFFF" REPORT "TEST 020.0 FAILED" SEVERITY ERROR;
		ASSERT zero = '0' REPORT "TEST 020.1 FAILED" SEVERITY ERROR;
		
		f <= "001"; a <= x"00000000"; b <= x"00000000";
		wait for 10ns;
		ASSERT y = x"00000000" REPORT "TEST 021.0 FAILED" SEVERITY ERROR;
		ASSERT zero = '1' REPORT "TEST 021.1 FAILED" SEVERITY ERROR;
		
		f <= "100"; a <= x"FFFFFFFF"; b <= x"FFFFFFFF";
		wait for 10ns;
		ASSERT y = x"00000000" REPORT "TEST 022.0 FAILED" SEVERITY ERROR;
		ASSERT zero = '1' REPORT "TEST 022.1 FAILED" SEVERITY ERROR;
		
		f <= "100"; a <= x"FFFFFFFF"; b <= x"12345678";
		wait for 10ns;
		ASSERT y = x"EDCBA987" REPORT "TEST 023.0 FAILED" SEVERITY ERROR;
		ASSERT zero = '0' REPORT "TEST 023.1 FAILED" SEVERITY ERROR;
		
		f <= "100"; a <= x"12345678"; b <= x"87654321";
		wait for 10ns;
		ASSERT y = x"10101458" REPORT "TEST 024.0 FAILED" SEVERITY ERROR;
		ASSERT zero = '0' REPORT "TEST 024.1 FAILED" SEVERITY ERROR;
		
		f <= "100"; a <= x"FFFFFFFF"; b <= x"00000000";
		wait for 10ns;
		ASSERT y = x"FFFFFFFF" REPORT "TEST 025.0 FAILED" SEVERITY ERROR;
		ASSERT zero = '0' REPORT "TEST 025.1 FAILED" SEVERITY ERROR;
		
		f <= "101"; a <= x"FFFFFFFF"; b <= x"FFFFFFFF";
		wait for 10ns;
		ASSERT y = x"FFFFFFFF" REPORT "TEST 026.0 FAILED" SEVERITY ERROR;
		ASSERT zero = '0' REPORT "TEST 026.1 FAILED" SEVERITY ERROR;
		
		f <= "101"; a <= x"12345678"; b <= x"87654321";
		wait for 10ns;
		ASSERT y = x"7ABEFEFE" REPORT "TEST 027.0 FAILED" SEVERITY ERROR;
		ASSERT zero = '0' REPORT "TEST 027.1 FAILED" SEVERITY ERROR;
		
		f <= "101"; a <= x"00000000"; b <= x"FFFFFFFF";
		wait for 10ns;
		ASSERT y = x"00000000" REPORT "TEST 028.0 FAILED" SEVERITY ERROR;
		ASSERT zero = '1' REPORT "TEST 028.1 FAILED" SEVERITY ERROR;
		
		f <= "101"; a <= x"00000000"; b <= x"00000000";
		wait for 10ns;
		ASSERT y = x"FFFFFFFF" REPORT "TEST 029.0 FAILED" SEVERITY ERROR;
		ASSERT zero = '0' REPORT "TEST 029.1 FAILED" SEVERITY ERROR;
		
      -- insert stimulus here 

      wait;
   end process;

END;
