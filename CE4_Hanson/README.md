# Computer Exercise #4 - 32-Bit ALU

## By C3C Tylar Hanson

## Table of Contents
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Preliminary Design](#preliminary-design)
3. [Software flow chart or algorithms](#software-flow-chart-or-algorithms)
4. [Hardware schematic](#hardware-schematic)
5. [Well-formatted code](#well-formatted-code)
6. [Debugging](#debugging)
7. [Testing methodology or results](#testing-methodology-or-results)
8. [Answers to Lab Questions](#answers-to-lab-questions)
9. [Observations and Conclusions](#observations-and-conclusions)
10. [Documentation](#documentation)
 
### Objectives or Purpose 
In this computer exercise, I will design, write, and test a 32-bit Arithmetic Logic Unit (ALU).  I will use this ALU in future assignments as I build a fully-functional MIPS microprocessor.  I will design and write a testbench of the ALU using VHDL.

### Preliminary design
These diagrams describe the hardware and functions I will create using behaioral VHDL code.

![](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/1fe08154be31ab7a4c793423b6326d0fc2d87d54/CE4_Hanson/prelim_design.JPG?token=19d7af6ebb9390d7a025f2f1a11022f30ab1cede)


### Hardware schematic
This is the top schematic of my ALU.

![](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/28da6a543254e3e97d02d53f906f49374760531a/CE4_Hanson/alu_schematic.JPG?token=33dcadc763d5182080f7257c6bcd1409728bd70a)

### Well-formatted code
Below is the VHDL code used to create the ALU.

	 --------------------------------------------------------------------
	-- Name: Tylar Hanson
	-- Date: 09 April 2016
	-- Course:	ECE_281
	-- File: nibble_to_sseg.vhd
	-- HW:	CE #4 32-Bit ALU
	--
	-- Purp: Design, write, and test a 32-bit Arithmetic Logic Unit (ALU)
	--
	-- Doc:	None
	--
	-- Academic Integrity Statement: I certify that, while others may have 
	-- assisted me in brain storming, debugging and validating this program, 
	-- the program itself is my own work. I understand that submitting code 
	-- which is the work of other individuals is a violation of the honor   
	-- code.  I also understand that if I knowingly give my original work to 
	-- another individual is also a violation of the honor code. 
	-------------------------------------------------------------------------
	library IEEE;
	use IEEE.STD_LOGIC_1164.ALL;
	use ieee.std_logic_signed.all;
	use IEEE.NUMERIC_STD.ALL;
	
	-- Uncomment the following library declaration if instantiating
	-- any Xilinx primitives in this code.
	--library UNISIM;
	--use UNISIM.VComponents.all;
	
	entity alu is
	    Port ( a, b : in  STD_LOGIC_VECTOR (31 downto 0);
	           f : in  STD_LOGIC_VECTOR (2 downto 0);
	           y : out  STD_LOGIC_VECTOR (31 downto 0);
	           zero : out  STD_LOGIC);
	end alu;
	
	architecture Behavioral of alu is
	
	signal temp : STD_LOGIC_VECTOR (31 downto 0);
	
	begin
	
		temp <= (a and b) when f = "000" else 
				  (a or b) when f = "001" else
				  (a + b) when f = "010" else
				  (a and (not b)) when f = "100" else
				  (a or (not b)) when f = "101" else
				  (a - b) when f = "110" else
				  x"00000001" when ( a < b ) and  f = "111" else
				  x"00000000";
		zero <= '1' when ( temp = x"00000000" ) else
				  '0';
		y <= temp;
	
	end Behavioral;   

### Debugging
My waveform simulation was not turning out as expected so after some thought and research I switched my ALU logic from a process to conditional statements. After doing that and correcting a few errors in my predicted outputs everything performed as expected.

### Testing methodology or results
Several test vectors were applied for each function of the ALU. Test vectors included inputs of all zero, all ones, and mixed. The expected outputs included all zeroes, all ones, and non-zero answers. Testing a few combinations of each ensures that almost all possible cases are working properly. This is a chart of all the test vectors I applied in order to test my ALU.

![](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/28da6a543254e3e97d02d53f906f49374760531a/CE4_Hanson/test_vectors.JPG?token=097b401214c9c3616c9847c9ffd1143a6070cbec)

The following are the results from the above test vectors. 

![](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/d395bc92ce80727732c36c9f15a2131102e6230c/CE4_Hanson/wavesim1.JPG?token=6b772ac3bd4f9d84ac9a75cd44651f963097ee72)

![](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/d395bc92ce80727732c36c9f15a2131102e6230c/CE4_Hanson/wavesim2.JPG?token=e31b97cb5730beeb0825c727b6ed4092d3d611ab)

![](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/d395bc92ce80727732c36c9f15a2131102e6230c/CE4_Hanson/wavesim3.JPG?token=5e067278cb886d87197811162ef803cb3c550d7d)

![](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/d395bc92ce80727732c36c9f15a2131102e6230c/CE4_Hanson/wavesim4.JPG?token=b79f5dd143507414432bc2540caff97604264938)![](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/d395bc92ce80727732c36c9f15a2131102e6230c/CE4_Hanson/wavesim4.2.JPG?token=f6b90bcd6b558c2a882c6966102cad50fce1a633)

The simulation results and my expected results were the same for each test so I know that this design is functional.

### Answers to Lab Questions
My ALU when built only required one adder and one comparator. Below is the synthesis report. By having fewer components my ALU can be smaller and faster. 

![](https://bytebucket.org/Hanson_Tylar/usafa_ece281_2016/raw/25eba78d333b518bddb11631cebd478f5bf11e0b/CE4_Hanson/synthesis_report.JPG?token=56b49766934243904ff82fc6bfd3c5fa0274a01d)

### Observations and Conclusions
The purpose of the computer exercise was to write VHDL code for an Arithmetic Logic Unit. Through comparing my expected test vectors to the simulation results waveform I can say this objective was completed. The code for the ALU turned out to be a little easier than I had originally thought. I am still not sure why writing it as a process wasn't working properly. I am interested to see how we use this module for other labs. 

### Documentation
None